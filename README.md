# workx

#### 介绍

#### 开发计划 version 0.0.1
## mybatis 扩展
### 1. 支持 event listener 隔离.    完成
### 2. 支持更细粒度的 event consumer , 比如只消费对某些表的操作触发的事件.    完成，目前支持针对特定的 mapper 来进行处理。
### 3. 支持和 spring 集成。
### 4. 支持和 spring boot 集成。
### 5. 提供一个通用 Mapper 接口实现单表的简单 crud.

## workx 自身
### 1. 支持 Timer 调用,可以对任何一段代码的调用加入超时限制功能


