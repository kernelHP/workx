package org.hepeng.workx.sqlparse;


import net.sf.jsqlparser.statement.select.SelectItemVisitor;

/**
 * @author he peng
 */
public interface SelectCountItemVisitor extends SelectItemVisitor {

    void visit(CountItem countItem);

}
