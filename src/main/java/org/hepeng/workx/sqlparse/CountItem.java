package org.hepeng.workx.sqlparse;

import net.sf.jsqlparser.parser.ASTNodeAccessImpl;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;

/**
 * @author he peng
 */
public class CountItem extends ASTNodeAccessImpl implements SelectItem {

    @Override
    public void accept(SelectItemVisitor selectItemVisitor) {
        ((SelectCountItemVisitor) selectItemVisitor).visit(this);
    }
}
