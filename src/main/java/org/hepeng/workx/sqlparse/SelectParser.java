package org.hepeng.workx.sqlparse;

import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.util.deparser.SelectDeParser;

/**
 * @author he peng
 */
public class SelectParser extends SelectDeParser implements SelectCountItemVisitor {

    public SelectParser() {}

    public SelectParser(ExpressionVisitor expressionVisitor, StringBuilder buffer) {
        super(expressionVisitor, buffer);
    }

    @Override
    public void visit(CountItem countItem) {
        super.buffer.append("COUNT(*)");
    }
}
