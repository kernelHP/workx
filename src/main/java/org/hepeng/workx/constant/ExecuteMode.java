package org.hepeng.workx.constant;

/**
 * @author he peng
 */
public enum ExecuteMode {

    SYNC , ASYNC
}
