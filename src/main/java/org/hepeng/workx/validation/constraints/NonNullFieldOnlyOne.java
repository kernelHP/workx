package org.hepeng.workx.validation.constraints;

import org.hepeng.workx.validation.validator.NonNullFieldOnlyOneValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author he peng
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {NonNullFieldOnlyOneValidator.class})
public @interface NonNullFieldOnlyOne {

    String message() default "there are multiple field that are not null";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    String[] fieldNames();
}
