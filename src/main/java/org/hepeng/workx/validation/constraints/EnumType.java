package org.hepeng.workx.validation.constraints;



import org.hepeng.workx.validation.validator.EnumTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 标识一个值属于某个枚举中的唯一的值
 * @author he peng
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {EnumTypeValidator.class})
public @interface EnumType {

    String message() default "value does not belong to enum";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    Class<? extends Enum> enumClass();

    String enumValueName();
}
