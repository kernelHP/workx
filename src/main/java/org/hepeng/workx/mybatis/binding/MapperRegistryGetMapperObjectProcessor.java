package org.hepeng.workx.mybatis.binding;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.hepeng.workx.util.ObjectProcessor;

/**
 * @author he peng
 */
public interface MapperRegistryGetMapperObjectProcessor extends ObjectProcessor<MapperRegistryGetMapperObjectProcessor.GetMapperPreProcessObject, MapperRegistryGetMapperObjectProcessor.GetMapperPostProcessObject> {

    @Builder
    @Getter
    @Setter
    class GetMapperPreProcessObject {
        private Configuration configuration;
        private Class<?> type;
        private SqlSession sqlSession;
    }

    @Builder
    @Getter
    @Setter
    class GetMapperPostProcessObject {
        private Class<?> type;
        private SqlSession sqlSession;
        private Object mapper;
    }
}
