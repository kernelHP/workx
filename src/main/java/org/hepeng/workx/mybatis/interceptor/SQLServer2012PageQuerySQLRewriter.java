package org.hepeng.workx.mybatis.interceptor;

/**
 * @author he peng
 */
public class SQLServer2012PageQuerySQLRewriter extends AbstractPageQuerySQLRewriter {

    @Override
    protected String doRewrite(String sql, Long startRow, Integer pageSize, Long endRow) throws Exception {
        StringBuilder builder = new StringBuilder(sql);
        builder.append(" OFFSET ")
                .append(startRow)
                .append(" ROWS FETCH NEXT ")
                .append(endRow)
                .append(" ROWS ONLY ");
        return builder.toString();
    }
}
