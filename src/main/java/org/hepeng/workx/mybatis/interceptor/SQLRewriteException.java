package org.hepeng.workx.mybatis.interceptor;

/**
 * @author he peng
 */
public class SQLRewriteException extends RuntimeException {

    public SQLRewriteException(String message) {
        super(message);
    }

    public SQLRewriteException(String message, Throwable cause) {
        super(message, cause);
    }

    public SQLRewriteException(Throwable cause) {
        super(cause);
    }

    public SQLRewriteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
