package org.hepeng.workx.mybatis.interceptor;

import org.hepeng.workx.extension.XPoint;

/**
 * @author he peng
 */

@XPoint("mysql")
public interface PageQuerySQLRewriter {

    String rewrite(String sql , Long startRow , Integer pageSize);

}
