package org.hepeng.workx.mybatis.interceptor;

/**
 * @author he peng
 */
public class MySQLPageQuerySQLRewriter extends AbstractPageQuerySQLRewriter {

    @Override
    protected String doRewrite(String sql, Long startRow, Integer pageSize, Long endRow) {
        return sql + " LIMIT " + startRow + " , " + pageSize;
    }
}
