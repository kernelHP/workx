package org.hepeng.workx.mybatis.interceptor;

import lombok.Data;
import org.hepeng.workx.util.PageResultSet;

import java.util.ArrayList;

/**
 * @author he peng
 */

@Data
public class PageList<T> extends ArrayList<T> {

    private PageResultSet pageResultSet;
}
