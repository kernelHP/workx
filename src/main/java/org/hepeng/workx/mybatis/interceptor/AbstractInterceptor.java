package org.hepeng.workx.mybatis.interceptor;

import org.apache.ibatis.plugin.Interceptor;

import java.util.Properties;

/**
 * @author he peng
 */
public abstract class AbstractInterceptor implements Interceptor {

    protected Properties properties;

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
