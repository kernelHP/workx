package org.hepeng.workx.mybatis.interceptor;

import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

/**
 * @author he peng
 */
public abstract class AbstractExecutorQueryInvokeInterceptor extends AbstractExecutorInvokeInterceptor {

    protected ResultHandler extractResultHandler(Invocation invocation) {
        return (ResultHandler) invocation.getArgs()[3];
    }

    protected RowBounds extractRowBounds(Invocation invocation) {
        return (RowBounds) invocation.getArgs()[2];
    }
}
