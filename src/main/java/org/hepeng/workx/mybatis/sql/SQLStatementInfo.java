package org.hepeng.workx.mybatis.sql;

import com.alibaba.druid.stat.TableStat;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.apache.ibatis.mapping.SqlCommandType;

import java.util.Collection;
import java.util.Map;

/**
 * @author he peng
 */

@Builder
@ToString
@Getter
public class SQLStatementInfo {

    private String sql;
    private SqlCommandType sqlCommandType;
    private Map<TableStat.Name, TableStat> tables;
    private Collection<TableStat.Column> columns;
}
