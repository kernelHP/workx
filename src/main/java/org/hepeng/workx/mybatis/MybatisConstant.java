package org.hepeng.workx.mybatis;

/**
 * @author he peng
 */
public final class MybatisConstant {

    public static final String MYBATIS_X_POINT_DIRECTORY = "mybatis";

    public static final String ENABLE_DATASOURCE_ROUTE = "enableDataSourceRoute";

    public static final String ENABLE_PUBLISH_EVENT = "enablePublishEvent";

    public static final String DATASOURCE_OPS_MODE = "mode";

    public static final String EVENT_CONSUMER_PACKAGES = "eventConsumerPackages";

    public static final String DATABASE_TYPE = "databaseType";
}
