package org.hepeng.workx.mybatis.util;

import org.apache.commons.lang3.StringUtils;
import org.hepeng.workx.jdbc.DataSourceMetadata;

import java.util.Objects;
import java.util.Properties;

import static org.hepeng.workx.jdbc.DataSourceMetadata.DataSourceOpsMode.*;

/**
 * @author he peng
 */
public class WorkXMybatisEnvironment {

    public static final String ENABLE_DATASOURCE_ROUTE = "enableDataSourceRoute";

    public static final String ENABLE_PUBLISH_EVENT = "enablePublishEvent";

    public static final String DATASOURCE_OPS_MODE = "mode";

    public static final String EVENT_CONSUMER_PACKAGES = "eventConsumerPackages";

    public static final String DIALECT = "dialect";

    public static final String LOGIC_NOT_DELETED = "logicNotDeleted";

    public static final String LOGIC_DELETED = "logicDeleted";

    private static Properties GLOBAL_PROPERTIES = new Properties();

    private static void refreshSettings(Properties props) {
        WorkXMybatisEnvironment.GLOBAL_PROPERTIES = props;
    }

    public static String getLogicNotDeleted() {
        return WorkXMybatisEnvironment.GLOBAL_PROPERTIES.getProperty(LOGIC_NOT_DELETED , "0");
    }

    public static String getLogicDeleted() {
        return WorkXMybatisEnvironment.GLOBAL_PROPERTIES.getProperty(LOGIC_DELETED , "1");
    }

    public static boolean isEnableDataSourceRoute(Properties props) {
        if (Objects.isNull(props)) {
            return Boolean.valueOf(WorkXMybatisEnvironment.GLOBAL_PROPERTIES.getProperty(ENABLE_DATASOURCE_ROUTE, "false"));
        }
        refreshSettings(props);
        return Boolean.valueOf(props.getProperty(ENABLE_DATASOURCE_ROUTE, "false"));
    }

    public static boolean isEnablePublishEvent(Properties props) {
        if (Objects.isNull(props)) {
            return Boolean.valueOf(WorkXMybatisEnvironment.GLOBAL_PROPERTIES.getProperty(ENABLE_PUBLISH_EVENT, "false"));
        }
        refreshSettings(props);
        return Boolean.valueOf(props.getProperty(ENABLE_PUBLISH_EVENT, "false"));
    }

    public static DataSourceMetadata.DataSourceOpsMode getDataSourceMode(Properties props) {
        if (Objects.isNull(props)) {
            String mode = WorkXMybatisEnvironment.GLOBAL_PROPERTIES.getProperty(DATASOURCE_OPS_MODE, READ_AND_WRITE.name());
            return DataSourceMetadata.DataSourceOpsMode.valueOf(StringUtils.upperCase(mode));
        }
        refreshSettings(props);
        String mode = props.getProperty(DATASOURCE_OPS_MODE, READ_AND_WRITE.name());
        return DataSourceMetadata.DataSourceOpsMode.valueOf(StringUtils.upperCase(mode));
    }

    public static String getEventConsumerPackages(Properties props) {
        if (Objects.isNull(props)) {
            return WorkXMybatisEnvironment.GLOBAL_PROPERTIES.getProperty(EVENT_CONSUMER_PACKAGES);
        }
        refreshSettings(props);
        return props.getProperty(EVENT_CONSUMER_PACKAGES);
    }

    public static String getDialect(Properties props) {
        if (Objects.isNull(props)) {
            return WorkXMybatisEnvironment.GLOBAL_PROPERTIES.getProperty(DIALECT);
        }
        refreshSettings(props);
        return props.getProperty(DIALECT);
    }
}
