package org.hepeng.workx.mybatis.event.publisher;

import org.hepeng.workx.mybatis.event.ExecuteEvent;
import org.hepeng.workx.mybatis.event.listener.ExecuteEventListener;

import java.util.Set;

/**
 * @author he peng
 */
public class NoOpExecuteEventPublisher implements ExecuteEventPublisher {

    @Override
    public void publishEvent(ExecuteEvent event) {
        System.err.println("execute event -> " + event);
    }

    @Override
    public void addEventListener(ExecuteEventListener listener) {

    }

    @Override
    public void addEventListener(Set<ExecuteEventListener> listeners) {

    }

    @Override
    public Set<ExecuteEventListener> getAllListener() {
        return null;
    }
}
