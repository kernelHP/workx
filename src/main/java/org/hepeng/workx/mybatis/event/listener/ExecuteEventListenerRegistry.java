package org.hepeng.workx.mybatis.event.listener;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.Set;

/**
 * @author he peng
 */
public class ExecuteEventListenerRegistry {

    private static final Set<ExecuteEventListener> EXECUTE_EVENT_LISTENERS = Sets.newConcurrentHashSet();

    public void addExecuteEventListener(ExecuteEventListener listener) {
        EXECUTE_EVENT_LISTENERS.add(listener);
    }

    public void addExecuteEventListener(Set<ExecuteEventListener> listeners) {
        EXECUTE_EVENT_LISTENERS.addAll(listeners);
    }

    public Set<ExecuteEventListener> getAllExecuteEventListeners() {
        return ImmutableSet.copyOf(EXECUTE_EVENT_LISTENERS);
    }

}
