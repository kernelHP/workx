package org.hepeng.workx.mybatis.event.listener;


/**
 * @author he peng
 */
public class DefaultExecuteEventListener extends AbstractExecuteEventListener {

    public DefaultExecuteEventListener() {}

    public DefaultExecuteEventListener(boolean isOnlySpecifiedPackage, String packageNames) {
        super(isOnlySpecifiedPackage, packageNames);
    }
}
