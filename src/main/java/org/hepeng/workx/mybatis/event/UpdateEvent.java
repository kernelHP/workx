package org.hepeng.workx.mybatis.event;

import org.hepeng.workx.mybatis.executor.SQLExecuteContext;

/**
 * @author he peng
 */
public class UpdateEvent extends ExecuteEvent {

    public UpdateEvent(long timestamp, Object payload, SQLExecuteContext context) {
        super(timestamp, payload , context);
    }

}
