package org.hepeng.workx.mybatis.event.listener;

import org.hepeng.workx.mybatis.event.consumer.EventConsumer;
import org.hepeng.workx.mybatis.event.ExecuteEvent;

import java.util.Set;

/**
 * @author he peng
 */
public interface ExecuteEventListener {

    void onExecuteEvent(ExecuteEvent event);

    void addEventConsumer(boolean isOnlySpecifiedPackage , String ... packages);

    void addEventConsumer(Class<?> key , EventConsumer consumer);

    void addEventConsumer(Class<?> key , Set<EventConsumer> consumers);

    Set<EventConsumer> getConsumers(Class<?> key);

    Set<EventConsumer> getConsumers(Class<?> key , boolean includeObjectClass);

}
