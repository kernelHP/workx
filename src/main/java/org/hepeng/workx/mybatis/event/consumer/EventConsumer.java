package org.hepeng.workx.mybatis.event.consumer;

import org.hepeng.workx.constant.ExecuteMode;

/**
 * @author he peng
 */
public interface EventConsumer<Event> {

    void onConsume(Event event);

    void onError(Throwable error);

    void onComplete();

    ExecuteMode getExecuteMode();

    int order();
}
