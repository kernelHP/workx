package org.hepeng.workx.mybatis.event.publisher;


import org.hepeng.workx.mybatis.event.listener.ExecuteEventListener;
import org.hepeng.workx.mybatis.event.listener.ExecuteEventListenerRegistry;

import java.util.Set;

/**
 * @author he peng
 */
public abstract class AbstractExecuteEventPublisher implements ExecuteEventPublisher {

    protected ExecuteEventListenerRegistry executeEventListenerRegistry = new ExecuteEventListenerRegistry();

    @Override
    public void addEventListener(ExecuteEventListener listener) {
        executeEventListenerRegistry.addExecuteEventListener(listener);
    }

    @Override
    public synchronized void addEventListener(Set<ExecuteEventListener> listeners) {
        executeEventListenerRegistry.addExecuteEventListener(listeners);
    }

    @Override
    public Set<ExecuteEventListener> getAllListener() {
        return executeEventListenerRegistry.getAllExecuteEventListeners();
    }
}
