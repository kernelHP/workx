package org.hepeng.workx.mybatis.event.consumer;

import org.hepeng.workx.constant.ExecuteMode;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author he peng
 */


@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface MyBatisEventConsumer {

    int order() default Integer.MAX_VALUE;

    ExecuteMode executeMode() default ExecuteMode.ASYNC;

}
