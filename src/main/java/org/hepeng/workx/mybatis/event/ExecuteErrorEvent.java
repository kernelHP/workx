package org.hepeng.workx.mybatis.event;

import org.hepeng.workx.mybatis.executor.SQLExecuteContext;

/**
 * @author he peng
 */
public class ExecuteErrorEvent extends ExecuteEvent {

    private final Throwable throwable;

    public ExecuteErrorEvent(long timestamp, Object payload , SQLExecuteContext context , Throwable throwable) {
        super(timestamp, payload , context);
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return this.throwable;
    }
}
