package org.hepeng.workx.mybatis.event;

import org.hepeng.workx.mybatis.executor.SQLExecuteContext;

/**
 * @author he peng
 */
public class TxRollbackEvent extends ExecuteEvent {

    public TxRollbackEvent(long timestamp, Object payload , SQLExecuteContext context) {
        super(timestamp, payload , context);
    }
}
