package org.hepeng.workx.mybatis.event.listener;

import org.hepeng.workx.mybatis.event.ExecuteEvent;
import org.hepeng.workx.mybatis.executor.SQLExecuteContext;

import java.util.Objects;

/**
 * @author he peng
 */
public class MapperExecuteEventListener extends AbstractExecuteEventListener {

    private Class<?> mapperInterface;

    public MapperExecuteEventListener(Class<?> mapperInterface) {
        this.mapperInterface = mapperInterface;
    }

    @Override
    public void onExecuteEvent(ExecuteEvent event) {
        SQLExecuteContext context = event.getSQLExecuteContext();
        Class<?> mapperInterface = context.getMapperInterface();
        if (Objects.equals(this.mapperInterface , mapperInterface)) {
            super.onExecuteEvent(event);
        }
    }
}
