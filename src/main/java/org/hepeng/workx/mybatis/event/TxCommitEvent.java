package org.hepeng.workx.mybatis.event;

import org.hepeng.workx.mybatis.executor.SQLExecuteContext;

/**
 * @author he peng
 */
public class TxCommitEvent extends ExecuteEvent {

    public TxCommitEvent(long timestamp, Object payload, SQLExecuteContext context) {
        super(timestamp, payload , context);
    }
}
