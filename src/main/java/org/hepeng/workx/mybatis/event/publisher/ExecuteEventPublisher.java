package org.hepeng.workx.mybatis.event.publisher;

import org.hepeng.workx.extension.XPoint;
import org.hepeng.workx.mybatis.event.ExecuteEvent;
import org.hepeng.workx.mybatis.event.listener.ExecuteEventListener;

import java.util.Set;

/**
 * @author he peng
 */

@XPoint("multicast")
public interface ExecuteEventPublisher {

    void publishEvent(ExecuteEvent event);

    void addEventListener(ExecuteEventListener listener);

    void addEventListener(Set<ExecuteEventListener> listeners);

    Set<ExecuteEventListener> getAllListener();

}
