package org.hepeng.workx.mybatis.event;

/**
 * @author he peng
 */

public interface MyBatisEvent {

    long getTimestamp();

    Object getPayload();
}
