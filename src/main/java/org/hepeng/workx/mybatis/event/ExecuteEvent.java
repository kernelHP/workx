package org.hepeng.workx.mybatis.event;

import org.hepeng.workx.mybatis.executor.SQLExecuteContext;

/**
 * @author he peng
 */
public abstract class ExecuteEvent implements MyBatisEvent {

    private final long timestamp;
    private final Object payload;
    private SQLExecuteContext SQLExecuteContext;

    public ExecuteEvent(long timestamp, Object payload , SQLExecuteContext context) {
        this.timestamp = timestamp;
        this.payload = payload;
        this.SQLExecuteContext = context;
    }

    @Override
    public long getTimestamp() {
        return this.timestamp;
    }

    @Override
    public Object getPayload() {
        return this.payload;
    }

    public SQLExecuteContext getSQLExecuteContext() {
        return this.SQLExecuteContext;
    }
}
