package org.hepeng.workx.mybatis.event;

import org.hepeng.workx.mybatis.executor.SQLExecuteContext;

/**
 * @author he peng
 */
public class DeleteEvent extends ExecuteEvent {

    public DeleteEvent(long timestamp, Object payload, SQLExecuteContext context) {
        super(timestamp, payload, context);
    }
}
