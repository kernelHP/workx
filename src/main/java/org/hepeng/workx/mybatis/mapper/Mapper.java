package org.hepeng.workx.mybatis.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author he peng
 */
public interface Mapper<T> {

    @InsertAllColumn
    Integer insert(T t);

    @InsertSelectiveColumn
    Integer insertSelective(T t);

    @UpdateAllColumn
    Integer updateById(T t);

    @UpdateSelectiveColumn
    Integer updateSelectiveById(T t);

    @DeleteAtPhysical
    Integer deleteByIdAtPhysical(@Param("primaryKey") Object id);

    @DeleteAtLogic
    Integer deleteByIdAtLogic(@Param("primaryKey") Object id);

    @SelectByPrimaryKey
    T selectOneById(@Param("primaryKey") Object id);

    @SelectByDynamicWhere
    List<T> selectByCondition(T t);
}
