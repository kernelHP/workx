package org.hepeng.workx.mybatis.spring.boot.autoconfigure;

import lombok.Data;
import org.hibernate.dialect.Database;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static org.hepeng.workx.mybatis.spring.boot.autoconfigure.WorkXMybatisProperties.WORKX_MYBATIS_PREFIX;

/**
 * @author he peng
 */

@ConfigurationProperties(prefix = WORKX_MYBATIS_PREFIX)
@Data
public class WorkXMybatisProperties {

    public static final String WORKX_MYBATIS_PREFIX = "workx.mybatis";

    private Boolean enable = false;

    private Boolean enableDataSourceRoute = false;

    private Boolean enablePublishEvent = false;

    private String eventConsumerPackages;

    private Database dialect;

}
