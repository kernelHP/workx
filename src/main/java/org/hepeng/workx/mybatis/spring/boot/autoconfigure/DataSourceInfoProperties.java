package org.hepeng.workx.mybatis.spring.boot.autoconfigure;

import lombok.Data;
import org.hepeng.workx.jdbc.DataSourceMetadata;

/**
 * @author he peng
 */

@Data
public class DataSourceInfoProperties {

    private String dataSourceName;
    private DataSourceMetadata.DataSourceOpsMode dataSourceOpsMode;
}
