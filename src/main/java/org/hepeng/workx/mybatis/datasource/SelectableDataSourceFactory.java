package org.hepeng.workx.mybatis.datasource;

import org.apache.ibatis.datasource.pooled.PooledDataSourceFactory;
import org.hepeng.workx.jdbc.SelectableDataSource;

import java.util.Properties;

/**
 * @author he peng
 */
public class SelectableDataSourceFactory extends PooledDataSourceFactory {

    public SelectableDataSourceFactory() {
        super.dataSource = new SelectableDataSource();
    }

    @Override
    public void setProperties(Properties properties) {
        try {
            super.setProperties(properties);
        } catch (Exception e) {

        }
    }
}
