package org.hepeng.workx.jdbc;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomUtils;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author he peng
 */
public class RandomReadAndWriteModeSelectDataSourceStrategy implements SelectDataSourceStrategy {

    @Override
    public DataSource chooseDataSource(Object key) {
        List<DataSource> readAndWriteDataSources = getReadAndWriteDataSources();
        if (CollectionUtils.isNotEmpty(readAndWriteDataSources)) {
            int i = RandomUtils.nextInt(0, readAndWriteDataSources.size());
            return readAndWriteDataSources.get(i);
        }
        return null;
    }
}
