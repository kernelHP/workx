package org.hepeng.workx.jdbc;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author he peng
 */
public class DataSourceMetadataStore {

    private static final Map<String , DataSourceMetadata> DATASOURCE_METADATA_MAP = new ConcurrentHashMap<>();

    public static void storeDataSourceMetaData(DataSourceMetadata dataSourceMetaData) {
        if (Objects.nonNull(dataSourceMetaData)) {
            DataSourceMetadataStore.DATASOURCE_METADATA_MAP.put(dataSourceMetaData.getId() , dataSourceMetaData);
        }
    }

    public static void storeDataSourceMetaData(List<DataSourceMetadata> dataSourceMetaData) {
        if (CollectionUtils.isNotEmpty(dataSourceMetaData)) {
            dataSourceMetaData.forEach(dataSourceInfo -> {
                if (Objects.nonNull(dataSourceInfo)) {
                    DataSourceMetadataStore.DATASOURCE_METADATA_MAP.put(dataSourceInfo.getId() , dataSourceInfo);
                }
            });
        }
    }

    public static List<DataSourceMetadata> getAllDataSourceMetaDatas() {
        return new ArrayList<>(Collections.unmodifiableCollection(
                DataSourceMetadataStore.DATASOURCE_METADATA_MAP.values()));
    }

    public static DataSourceMetadata getDataSourceMetaData(String id) {
        return DataSourceMetadataStore.DATASOURCE_METADATA_MAP.get(id);
    }
}
