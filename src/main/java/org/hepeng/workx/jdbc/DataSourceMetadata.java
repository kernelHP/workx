package org.hepeng.workx.jdbc;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.sql.DataSource;
import java.sql.DatabaseMetaData;

/**
 * @author he peng
 */

@Getter
@Setter
@ToString
public class DataSourceMetadata {

    private String id;
    private DataSource dataSource;
    private DataSourceOpsMode mode;
    private DatabaseMetaData databaseMetaData;

    public DataSourceMetadata() {}

    public DataSourceMetadata(String id, DataSource dataSource, DataSourceOpsMode mode) throws Exception {
        this.id = id;
        this.dataSource = dataSource;
        this.mode = mode;
        this.databaseMetaData = dataSource.getConnection().getMetaData();
    }

    public DataSourceMetadata(String id, DataSource dataSource, DataSourceOpsMode mode, DatabaseMetaData databaseMetaData) {
        this.id = id;
        this.dataSource = dataSource;
        this.mode = mode;
        this.databaseMetaData = databaseMetaData;
    }

    public enum DataSourceOpsMode {
        READ_ONLY , WRITE_ONLY , READ_AND_WRITE
    }
}
