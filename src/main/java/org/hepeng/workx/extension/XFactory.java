package org.hepeng.workx.extension;

import java.util.List;

/**
 * @author he peng
 */
public interface XFactory {

    <T> T getX(Class<T> xPointClass, String xName);

    <T> T getX(Class<T> xPointClass, String xName , List<Class<?>> constructorArgTypes, List<Object> constructorArgs);

}
