package org.hepeng.workx.web.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * http 接口响应对象
 * @author he peng
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HttpResponsePayload {

    private Integer errorCode;
    private String errorMsg;
    private String token;
    private Object entity;
}
