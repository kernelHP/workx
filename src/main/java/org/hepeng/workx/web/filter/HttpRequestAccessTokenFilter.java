package org.hepeng.workx.web.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * http 请求访问 token 校验过滤器 .
 * 防止接口重复提交 , 防止部分恶意攻击.
 * @author he peng
 */

@Deprecated
public class HttpRequestAccessTokenFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(HttpRequestAccessTokenFilter.class);
    private Set<String> ignoreMethods;

    public HttpRequestAccessTokenFilter(Set<String> ignoreMethods) {
        this.ignoreMethods = ignoreMethods;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // nothing to do
        chain.doFilter(request , response);
    }

    @Override
    public void destroy() {

    }
}
