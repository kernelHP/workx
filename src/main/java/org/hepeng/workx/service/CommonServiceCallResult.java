package org.hepeng.workx.service;


import lombok.Getter;
import org.hepeng.workx.service.error.Error;

import java.util.Objects;

/**
 *
 * @author he peng
 */

@Getter
public class CommonServiceCallResult<E extends Error,D extends Object> implements ServiceCallResult {

    protected E error;
    protected D data;
    protected String msg;

    protected CommonServiceCallResult() {}

    @Override
    public Boolean isSuccess() {
        if (Objects.equals(this.error.getCode() , Error.SUCCESS.getCode())) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "CommonServiceCallResult{" +
                "error=" + error +
                ", data=" + data +
                '}';
    }

    public static ServiceCallResultBuilder builder() {
        return new ServiceCallResultBuilder();
    }

    public static class ServiceCallResultBuilder<E extends Error,D extends Object> {

        private CommonServiceCallResult commonServiceCallResult = new CommonServiceCallResult();

        public ServiceCallResultBuilder error(E error) {
            commonServiceCallResult.error = error;
            msg(error.getMsg());
            return this;
        }

        public ServiceCallResultBuilder data(D data) {
            commonServiceCallResult.data = data;
            return this;
        }

        public ServiceCallResultBuilder msg(String errorMsg) {
            commonServiceCallResult.msg = errorMsg;
            return this;
        }

        public ServiceCallResult build() {
            return commonServiceCallResult;
        }

    }
}
