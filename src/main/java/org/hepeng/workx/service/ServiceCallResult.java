package org.hepeng.workx.service;

import org.hepeng.workx.service.error.Error;

import java.io.Serializable;

/**
 * 服务调用结果顶级接口
 * @author he peng
 */
public interface ServiceCallResult<E extends Error, D extends Object> extends Serializable {

    /**
     * 检测服务调用是否成功.
     * @return 返回 true - 表示服务调用成功； false - 表示服务调用失败；
     */
    Boolean isSuccess();

    /**
     * 获取服务调用结果中的错误信息
     * @return 返回一个服务错误实例对象
     */
    E getError();

    /**
     * 获取服务调用后的结果数据
     * @return 返回一个数据对象
     */
    D getData();

    String getMsg();
}
