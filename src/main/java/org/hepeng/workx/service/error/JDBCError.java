package org.hepeng.workx.service.error;

/**
 * @author he peng
 */
public interface JDBCError extends DataAccessError {

    Error JDBC_ERROR = ErrorLoader.loadError("jdbc.error");

    Error SQL_ERROR = ErrorLoader.loadError("jdbc.sql.error");

    Error TRANSACTION_ERROR = ErrorLoader.loadError("jdbc.transaction.error");

    Error CONCURRENCY_ERROR = ErrorLoader.loadError("jdbc.concurrency.error");

    Error DUPLICATE_KEY_ERROR = ErrorLoader.loadError("jdbc.duplicateKey.error");

    Error CONNECTION_ERROR = ErrorLoader.loadError("jdbc.connection.error");


}
