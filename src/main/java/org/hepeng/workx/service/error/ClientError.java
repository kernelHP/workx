package org.hepeng.workx.service.error;

/**
 * @author he peng
 */
public interface ClientError extends Error {

    Error CLIENT_ERROR = ErrorLoader.loadError("client.error");

    Error PARAMETER_ERROR = ErrorLoader.loadError("client.parameter.error");

    Error AUTH_ERROR = ErrorLoader.loadError("client.auth.error");
}
