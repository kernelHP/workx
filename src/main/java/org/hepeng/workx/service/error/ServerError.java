package org.hepeng.workx.service.error;

/**
 * @author he peng
 */
public interface ServerError extends DataAccessError {

    Error SERVER_ERROR = ErrorLoader.loadError("server.error");
}
