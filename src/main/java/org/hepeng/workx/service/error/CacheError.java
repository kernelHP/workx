package org.hepeng.workx.service.error;

/**
 * @author he peng
 */
public interface CacheError extends DataAccessError {

    Error CACHE_ERROR = ErrorLoader.loadError("cache.error");

}
