package org.hepeng.workx.service.error;

import java.io.Serializable;

/**
 * 服务错误码顶级接口
 * @author he peng
 */
public interface Error extends Serializable {

    Error SUCCESS = ErrorLoader.loadError("success");
    Error UNKNOWN_ERROR = ErrorLoader.loadError("unknown.error");

    /**
     * 获取错误码
     * @return 返回错误码
     */
    Integer getCode();

    /**
     * 获取错误信息
     * @return  返回错误信息
     */
    String getMsg();
}
