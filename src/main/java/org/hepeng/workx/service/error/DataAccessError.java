package org.hepeng.workx.service.error;

/**
 * @author he peng
 */
public interface DataAccessError extends Error {

    Error DATA_ACCESS_ERROR = ErrorLoader.loadError("data.access.error");

}
