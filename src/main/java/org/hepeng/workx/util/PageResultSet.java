package org.hepeng.workx.util;

import lombok.Data;

import java.util.List;

/**
 * @author he peng
 */

@Data
public class PageResultSet<T> {

    private Long startRow;
    private Long startPage;
    private Integer pageSize;
    private Long totalRow;
    private Long totalPage;
    private List<T> records;

    public void setTotalRow(Long totalRow) {
        this.totalRow = totalRow;
        this.totalPage = calculateTotalPage();
    }

    private Long calculateTotalPage() {
        Long pages;
        if (this.totalRow <= this.pageSize) {
            pages = 1L;
        } else {
            pages = this.totalRow / this.pageSize;
            long residue = this.totalRow % this.pageSize;
            if (residue > 0) {
                pages += 1;
            }
        }
        return pages;
    }
}
