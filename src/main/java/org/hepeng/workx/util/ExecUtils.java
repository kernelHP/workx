package org.hepeng.workx.util;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author he peng
 */
public class ExecUtils {

    public static List<String> exec(String cmd) throws IOException {
        Process process = Runtime.getRuntime().exec(cmd);
        return IOUtils.readLines(process.getInputStream(), Charset.forName("utf-8"));
    }

}
