package org.hepeng.workx.util;

import lombok.Data;

import java.util.Objects;

/**
 * @author he peng
 */

@Data
public class PageQuery {

    private static final Long DEFAULT_START_ROW = 0L;
    private static final Long DEFAULT_START_PAGE = 0L;
    private static final Integer DEFAULT_PAGE_SIZE = 15;

    private Long startRow = DEFAULT_START_ROW;
    private Long startPage = DEFAULT_START_PAGE;
    private Integer pageSize = DEFAULT_PAGE_SIZE;

    public PageQuery() {}

    public PageQuery(Long startPage, Integer pageSize) {
        if (Objects.nonNull(startPage) || startPage > 0) {
            this.startPage = startPage;
        }

        if (Objects.nonNull(pageSize) || pageSize > 0) {
            this.pageSize = pageSize;
        }

        this.startRow = this.startPage * this.pageSize;
    }
}
