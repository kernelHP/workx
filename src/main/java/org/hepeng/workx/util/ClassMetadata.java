package org.hepeng.workx.util;

/**
 * @author he peng
 */
public interface ClassMetadata {

    Class getJavaClass();

    String getClassName();

    Boolean isInterface();

    Boolean isAnnotation();

    Boolean isAbstract();

    Boolean isConcrete();

    Boolean isFinal();

    String getSuperClassName();

    String[] getInterfaceNames();
}
