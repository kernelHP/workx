package org.hepeng.workx.util.timer;

/**
 * @author he peng
 */

@FunctionalInterface
public interface Function<R> {

    R call();
}
