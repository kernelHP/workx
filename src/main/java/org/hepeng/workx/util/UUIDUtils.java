package org.hepeng.workx.util;

import java.util.UUID;

/**
 * @author he peng
 */
public class UUIDUtils {

    private UUIDUtils() {}

    /**
     * 生成 32 位长度的 uuid 字符串
     * @return 返回长度为 32 位的 uuid 字符串
     */
    public static String uuid32() {
        return UUID.randomUUID().toString().replaceAll("-" , "");
    }
}
