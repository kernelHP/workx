package org.hepeng.workx.util;


import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author he peng
 */
public class CompositeObjectProcessor implements ObjectProcessor<Object , Object> {

    private final List<ObjectProcessor> objectProcessors = Collections.synchronizedList(new ArrayList<>());

    @Override
    public Object preProcess(Object obj) {
        for (ObjectProcessor objectProcessor : objectProcessors) {
            obj = objectProcessor.preProcess(obj);
        }
        return obj;
    }

    @Override
    public Object postProcess(Object obj) {
        for (ObjectProcessor objectProcessor : objectProcessors) {
            obj = objectProcessor.postProcess(obj);
        }
        return obj;
    }

    public void addObjectProcessor(ObjectProcessor processor) {
        objectProcessors.add(processor);
    }

}
