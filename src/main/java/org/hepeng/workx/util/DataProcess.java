package org.hepeng.workx.util;

/**
 * @author he peng
 */
public interface DataProcess<T , R> {

    R process(T t);
}
