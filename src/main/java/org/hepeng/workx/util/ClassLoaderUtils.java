package org.hepeng.workx.util;

/**
 * @author he peng
 */
public class ClassLoaderUtils {

    private ClassLoaderUtils() {}

    /**
     * 获取当前线程的 {@link ClassLoader} 实例
     * @return 返回当前线程的 {@link ClassLoader} 实例
     */
    public static ClassLoader getCurrentThreadClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    public static ClassLoader getSystemClassLoader() {
        return ClassLoader.getSystemClassLoader();
    }
}
