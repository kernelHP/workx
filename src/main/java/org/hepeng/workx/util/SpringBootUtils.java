package org.hepeng.workx.util;

import org.apache.commons.lang3.StringUtils;
import org.hepeng.workx.exception.ApplicationRuntimeException;

/**
 * @author he peng
 */
public class SpringBootUtils {

    private SpringBootUtils() {}

    /**
     * 获取一个 classpath 下的 spring 配置文件的配置项字符串。
     * 例如 ：spring.config.location=classpath:/application-api-dev.properties,classpath:/application-core-dev.properties,
     * @param configFiles 配置文件名称
     * @return 返回例如：例如 ：spring.config.location=classpath:/application-api-dev.properties,classpath:/application-core-dev.properties,
     */
    public static String getSpringConfigLocationFromClassPath(String ... configFiles) {
        String activeProfiles = getActiveProfiles();
        String springConfigLocation = "spring.config.location=";
        if (StringUtils.isBlank(activeProfiles)) {
            for (String configFile : configFiles) {
                springConfigLocation += "classpath:/" + configFile + ",";
            }
        } else {
            for (String configFile : configFiles) {
                int i = configFile.lastIndexOf(".");
                String configFileName = configFile.substring(0, i);
                String fileExtension = configFile.substring(i + 1);
                String newConfigFile = configFileName + "-" + activeProfiles + "." + fileExtension;
                springConfigLocation += "classpath:/" + newConfigFile + ",";
            }
        }
        return springConfigLocation;
    }

    /**
     * 获取当前激活的 profiles
     * @return 返回当前激活的 profiles 名称
     */
    public static String getActiveProfiles() {
        return System.getProperty("spring.profiles.active");
    }

    /**
     * 检查是否指定了 active profiles ，如果未指定则会抛出异常
     */
    public static void checkActiveProfiles() {
        if (StringUtils.isBlank(getActiveProfiles())) {
            throw new ApplicationRuntimeException("active profiles not specified , " +
                    "please use [spring.profiles.active] startup parameter designation active profiles");
        }
    }

}
