package org.hepeng.workx.util;

import org.reflections.util.ClasspathHelper;

import java.net.URL;
import java.util.Collection;

/**
 * @author he peng
 */
public class ClassPathUtils {

    public static Collection<URL> forJavaClassPath() {
        return ClasspathHelper.forJavaClassPath();
    }
}
