package org.hepeng.workx.util.serializer;

import com.caucho.hessian.io.Hessian2StreamingInput;
import com.caucho.hessian.io.Hessian2StreamingOutput;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * @author he peng
 */
public class Hessian2ObjectSerializer<T> extends ExceptionTranslationObjectSerializer<T> {


    @Override
    protected byte[] convert(T object) throws Exception {
        ObjectSerializationUtils.serializableCheck(object);
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        Hessian2StreamingOutput streamingOutput = new Hessian2StreamingOutput(byteOutputStream);
        streamingOutput.writeObject(object);
        streamingOutput.flush();
        return byteOutputStream.toByteArray();
    }

    @Override
    protected T convert(byte[] bytes) throws Exception {
        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes);
        Hessian2StreamingInput streamingInput = new Hessian2StreamingInput(byteInputStream);
        return (T) streamingInput.readObject();
    }
}
