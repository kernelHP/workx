package org.hepeng.workx.util.serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.pool.KryoFactory;
import org.objenesis.strategy.StdInstantiatorStrategy;

/**
 * @author he peng
 */
public class KryoFactoryImpl implements KryoFactory {

    @Override
    public Kryo create() {
        Kryo kryo = new Kryo();
        kryo.setInstantiatorStrategy(new StdInstantiatorStrategy());
        return kryo;
    }
}
