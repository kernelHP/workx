package org.hepeng.workx.util.serializer;

import org.hepeng.workx.exception.ApplicationRuntimeException;

/**
 * @author he peng
 */
public class SerializationException extends ApplicationRuntimeException {

    public SerializationException(String message) {
        super(message);
    }

    public SerializationException(Throwable cause) {
        super(cause);
    }

    public SerializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
