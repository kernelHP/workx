package org.hepeng.workx.util.serializer;

import org.apache.commons.codec.binary.Base64;

/**
 * @author he peng
 */
@Deprecated
public class Base64ObjectSerializer implements ObjectSerializer<Object> {

    private ObjectSerializer delegate;

    public Base64ObjectSerializer(ObjectSerializer objectSerializer) {
        this.delegate = objectSerializer;
    }

    @Override
    public byte[] serialize(Object object) throws SerializationException {
        return delegate.serialize(object);
    }

    @Override
    public String serializeBase64String(Object object) throws SerializationException {
        return Base64.encodeBase64String(serialize(object));
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        return delegate.deserialize(bytes);
    }

    @Override
    public Object deserializeBase64String(String base64Str) throws SerializationException {
        return deserialize(Base64.decodeBase64(base64Str));
    }
}
