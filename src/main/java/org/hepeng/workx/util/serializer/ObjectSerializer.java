package org.hepeng.workx.util.serializer;


/**
 * @author he peng
 */
public interface ObjectSerializer<T> {

    byte[] serialize(T t) throws SerializationException;

    String serializeBase64String(T t) throws SerializationException;

    T deserialize(byte[] bytes) throws SerializationException;

    T deserializeBase64String(String base64Str) throws SerializationException;

}
