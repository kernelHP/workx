package org.hepeng.workx.util.serializer;

/**
 * @author he peng
 */
public enum SupportSerializer {

    JDK,

    HESSIAN,

    KRYO;
}
