package org.hepeng.workx.util;



import org.hepeng.workx.exception.ApplicationRuntimeException;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * 枚举类型工具类
 * @author he peng
 */
public class EnumUtils {

    private EnumUtils() {}

    public static <E extends Enum<E>> E valueOf(Class<E> eClass , String valFiledName , Object val) {

        E en = null;
        try {
            for (Object e : org.apache.commons.lang3.EnumUtils.getEnumList(eClass)) {
                Field field = eClass.getDeclaredField(valFiledName);
                field.setAccessible(true);
                Object obj = field.get(e);

                if (Number.class.isAssignableFrom(obj.getClass())) {
                    obj = String.valueOf(obj);
                    val = String.valueOf(val);
                }
                if (Objects.equals(obj , val)) {
                    en = (E) e;
                    break;
                }
            }
        } catch (Throwable t) {
            throw new ApplicationRuntimeException(t);
        }
        return en;
    }
}
