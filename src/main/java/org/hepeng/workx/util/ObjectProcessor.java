package org.hepeng.workx.util;

/**
 * @author he peng
 */
public interface ObjectProcessor<Pre,Post> {

    Pre preProcess(Pre preProcessObject);

    Post postProcess(Post postProcessObject);

}
