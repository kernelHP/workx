package org.hepeng.workx.util.proxy;

import org.hepeng.workx.extension.XPoint;

import java.util.List;

/**
 * @author he peng
 */

@XPoint("javassist")
public interface ProxyFactory {

    Object createProxy(Object target , List<Class<?>> interfaces , List<Invoker> invokers , List<InvokeFilter> filters);

    Object createProxy(Class targetClass , List<Class<?>> interfaces ,
                       List<Class<?>> constructorArgTypes, List<Object> constructorArgs ,
                       List<Invoker> invokers , List<InvokeFilter> filters);

}