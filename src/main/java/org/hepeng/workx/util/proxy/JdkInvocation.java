package org.hepeng.workx.util.proxy;

import lombok.Getter;

import java.lang.reflect.Method;

/**
 * @author he peng
 */

@Getter
public class JdkInvocation extends AbstractInvocation {

    public JdkInvocation(Object nativeObject , Object proxy, Method method, Object[] args) {
        super(nativeObject , proxy, method, args);
    }
}
