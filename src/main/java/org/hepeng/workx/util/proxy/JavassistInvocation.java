package org.hepeng.workx.util.proxy;

import lombok.Getter;

import java.lang.reflect.Method;

/**
 * @author he peng
 */

@Getter
public class JavassistInvocation extends AbstractInvocation {

    protected Method proceedMethod;

    public JavassistInvocation(Object nativeObject , Object proxy, Method method, Object[] args , Method proceedMethod) {
        super(nativeObject , proxy, method, args);
        this.proceedMethod = proceedMethod;
    }
}
