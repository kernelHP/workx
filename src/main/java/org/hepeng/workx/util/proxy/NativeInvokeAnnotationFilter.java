package org.hepeng.workx.util.proxy;

import org.apache.commons.lang3.reflect.MethodUtils;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 * @author he peng
 */
public class NativeInvokeAnnotationFilter implements InvokeFilter {

    @Override
    public boolean isProxyInvoke(Method method) {
        NativeInvoke ann = MethodUtils.getAnnotation(method, NativeInvoke.class, true, true);
        return Objects.isNull(ann);
    }
}
