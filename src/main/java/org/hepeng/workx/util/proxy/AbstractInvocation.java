package org.hepeng.workx.util.proxy;


import java.lang.reflect.Method;

/**
 * @author he peng
 */
public abstract class AbstractInvocation implements Invocation {

    protected Object nativeObject;
    protected Object proxy;
    protected Method method;
    protected Object[] args;

    public AbstractInvocation(Object nativeObject , Object proxy, Method method, Object[] args) {
        this.nativeObject = nativeObject;
        this.proxy = proxy;
        this.method = method;
        this.args = args;
    }

    @Override
    public Object invoke() throws Throwable {
        return this.method.invoke(this.nativeObject , this.args);
    }

    @Override
    public Object getNative() {
        return this.nativeObject;
    }

    @Override
    public Object getProxy() {
        return this.proxy;
    }

    @Override
    public Method getMethod() {
        return this.method;
    }

    @Override
    public Object[] getArgs() {
        return this.args;
    }
}
