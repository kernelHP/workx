package org.hepeng.workx.util.proxy;

import java.lang.reflect.Method;

/**
 * @author he peng
 */
public interface Invocation {

    Object getNative();

    Object getProxy();

    Method getMethod();

    Object[] getArgs();

    Object invoke() throws Throwable;
}
