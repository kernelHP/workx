package org.hepeng.workx.util.proxy;

import java.lang.reflect.Method;

/**
 * @author he peng
 */
public interface InvokeFilter {

    boolean isProxyInvoke(Method method);
}
