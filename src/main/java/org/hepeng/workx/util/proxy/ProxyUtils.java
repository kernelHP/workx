package org.hepeng.workx.util.proxy;

import javassist.util.proxy.ProxyFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import java.lang.reflect.Proxy;

/**
 * @author he peng
 */
public class ProxyUtils {

    public static boolean isJdkProxy(Class<?> clazz) {
        Assert.notNull(clazz , "clazz must not be null");
        return Proxy.isProxyClass(clazz);
    }

    public static boolean isJavassistProxy(Class<?> clazz) {
        Assert.notNull(clazz , "clazz must not be null");
        return ProxyFactory.isProxyClass(clazz);
    }

    public static boolean isCglibProxy(Class<?> clazz) {
        Assert.notNull(clazz , "clazz must not be null");
        return net.sf.cglib.proxy.Proxy.isProxyClass(clazz) || ClassUtils.isCglibProxyClass(clazz);
    }

    public static boolean isProxy(Class<?> clazz) {
        Assert.notNull(clazz , "clazz must not be null");
        return isJdkProxy(clazz) || isCglibProxy(clazz) || isJavassistProxy(clazz);
    }
}
