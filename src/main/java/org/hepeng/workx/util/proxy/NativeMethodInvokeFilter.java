package org.hepeng.workx.util.proxy;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * @author he peng
 */
public class NativeMethodInvokeFilter implements InvokeFilter {

    @Override
    public boolean isProxyInvoke(Method method) {
        return ! Modifier.isNative(method.getModifiers());
    }
}
