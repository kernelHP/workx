package org.hepeng.workx.util.proxy;

import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;

import java.lang.reflect.Method;
import java.util.Set;

/**
 * @author he peng
 */
public class TargetProxyMethodInvokerFilter implements InvokeFilter {

    private Set<Method> targetProxyMethods = Sets.newConcurrentHashSet();

    public TargetProxyMethodInvokerFilter(Set<Method> targetProxyMethods) {
        if (CollectionUtils.isNotEmpty(targetProxyMethods)) {
            this.targetProxyMethods.addAll(targetProxyMethods);
        }
    }

    @Override
    public boolean isProxyInvoke(Method method) {
        return this.targetProxyMethods.contains(method);
    }
}
