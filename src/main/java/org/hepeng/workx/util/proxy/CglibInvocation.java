package org.hepeng.workx.util.proxy;

import lombok.Getter;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author he peng
 */

@Getter
public class CglibInvocation extends AbstractInvocation {

    protected MethodProxy methodProxy;

    public CglibInvocation(Object nativeObject , Object proxy, Method method, Object[] args , MethodProxy methodProxy) {
        super(nativeObject , proxy, method, args);
        this.methodProxy = methodProxy;
    }

}
