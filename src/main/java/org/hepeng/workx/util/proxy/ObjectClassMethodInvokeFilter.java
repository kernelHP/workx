package org.hepeng.workx.util.proxy;

import java.lang.reflect.Method;

/**
 * @author he peng
 */
public class ObjectClassMethodInvokeFilter implements InvokeFilter {

    @Override
    public boolean isProxyInvoke(Method method) {
        return ! Object.class.equals(method.getDeclaringClass());
    }
}
