package org.hepeng.workx.util.proxy;


/**
 * @author he peng
 */
public interface Invoker {

    Object invoke(Invocation invocation) throws Throwable;

}
