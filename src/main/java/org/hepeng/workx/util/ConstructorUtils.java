package org.hepeng.workx.util;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * @author he peng
 */
public class ConstructorUtils {

    public static Class<?>[] argTypeToArray(List<Class<?>> constructorArgTypes) {
        Class<?>[] argTypes;
        if (CollectionUtils.isEmpty(constructorArgTypes)) {
            argTypes = new Class<?>[0];
        } else {
            argTypes = new Class<?>[constructorArgTypes.size()];
            argTypes = constructorArgTypes.toArray(argTypes);
        }
        return argTypes;
    }

    public static Object[] argToArray(List<Object> constructorArgs) {
        Object[] args;
        if (CollectionUtils.isEmpty(constructorArgs)) {
            args = new Object[0];
        } else {
            args = new Object[constructorArgs.size()];
            args = constructorArgs.toArray(args);
        }

        return args;
    }
}
