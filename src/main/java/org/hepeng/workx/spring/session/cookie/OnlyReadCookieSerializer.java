package org.hepeng.workx.spring.session.cookie;

import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * @author he peng
 */
public class OnlyReadCookieSerializer extends DefaultCookieSerializer {

    @Override
    public void writeCookieValue(CookieValue cookieValue) {

    }
}
