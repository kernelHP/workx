package org.hepeng.workx.spring.session.redis.serializer;

import org.springframework.data.redis.core.StringRedisTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * @author he peng
 */
public class SafetySessionRepositoryRequestWrapper extends HttpServletRequestWrapper {

    private StringRedisTemplate redisTemplate;

    public SafetySessionRepositoryRequestWrapper(HttpServletRequest request, StringRedisTemplate redisTemplate) {
        super(request);
        this.redisTemplate = redisTemplate;
    }

    @Override
    public HttpSession getSession(boolean create) {
        HttpSession httpSession = ((HttpServletRequest) super.getRequest()).getSession(create);
        if ( ! create && Objects.isNull(httpSession)) {
            return null;
        }
        return new SafetyHttpSessionWrapper(httpSession , redisTemplate);
    }

    @Override
    public HttpSession getSession() {
        HttpSession httpSession = ((HttpServletRequest) super.getRequest()).getSession();
        return new SafetyHttpSessionWrapper(httpSession , redisTemplate);
    }
}
