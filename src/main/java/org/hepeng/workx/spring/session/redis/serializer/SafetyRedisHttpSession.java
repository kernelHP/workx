package org.hepeng.workx.spring.session.redis.serializer;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author he peng
 */

@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({ java.lang.annotation.ElementType.TYPE })
@Documented
@Import(SafetyRedisSerializerConfiguration.class)
public @interface SafetyRedisHttpSession {
}
