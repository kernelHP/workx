package org.hepeng.workx.spring.session.redis.serializer;


import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.session.web.http.SessionRepositoryFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author he peng
 */
public class SafetySessionRepositoryFilter extends OncePerRequestFilter {

    public static final int DEFAULT_ORDER = SessionRepositoryFilter.DEFAULT_ORDER + 1;

    private StringRedisTemplate redisTemplate;

    public SafetySessionRepositoryFilter(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        filterChain.doFilter(new SafetySessionRepositoryRequestWrapper(request , redisTemplate) , response);
    }
}
