package org.hepeng.workx.spring.session.redis.serializer;
import java.io.File;

import org.hepeng.workx.spring.data.redis.serializer.SecurityModuleGenericJackson2JsonRedisSerializer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author he peng
 */

@Configuration
public class SafetyRedisSerializerConfiguration {

    public static final String TOPIC = "SafetyRedisSerializerTopic";

    public static final String CLASSES_DIR = System.getProperty("java.io.tmpdir") + File.separatorChar + "classes" + File.separatorChar;

    @Bean
    public FilterRegistrationBean safetySessionRepositoryFilter(StringRedisTemplate redisTemplate) {
        SafetySessionRepositoryFilter safetySessionRepositoryFilter = new SafetySessionRepositoryFilter(redisTemplate);
        FilterRegistrationBean frb = new FilterRegistrationBean();
        frb.addUrlPatterns("/*");
        frb.setName("safetySessionRepositoryFilter");
        frb.setOrder(SafetySessionRepositoryFilter.DEFAULT_ORDER);
        frb.setFilter(safetySessionRepositoryFilter);
        return frb;
    }

    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                                   MessageListenerAdapter listenerAdapter){
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter,new PatternTopic(TOPIC));
        return container;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(SafetyRedisHttpSessionMessageReceiver receiver){
        return new MessageListenerAdapter(receiver ,"receiveMessage");
    }

    @Bean
    public SafetyRedisHttpSessionMessageReceiver safetyRedisHttpSessionMessageReceiver(RedisSerializer serializer) {
        SafetyRedisHttpSessionMessageReceiver receiver = new JdkSerializationSafetyClassBytesHandler(serializer);
        if (GenericJackson2JsonRedisSerializer.class.isAssignableFrom(serializer.getClass())) {
            receiver = new GenericJackson2SafetyClassBytesHandler(serializer);
        }
        return receiver;
    }

    @Bean("springSessionDefaultRedisSerializer")
    public RedisSerializer<Object> redisSerializer() {
        return SafetyRedisSerializerUtils.safeJdkRedisSerializer(new JdkSerializationRedisSerializer());
    }
}
