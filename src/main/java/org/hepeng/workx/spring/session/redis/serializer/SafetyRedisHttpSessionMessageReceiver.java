package org.hepeng.workx.spring.session.redis.serializer;

/**
 * @author he peng
 */
public interface SafetyRedisHttpSessionMessageReceiver {

    void receiveMessage(String message) throws Exception;

}

