package org.hepeng.workx.spring.security.web.config;

import org.hepeng.workx.spring.security.web.DynamicSecurityIgnoringFilterChainProxy;
import org.springframework.context.annotation.Bean;


/**
 * @author he peng
 */

public class DynamicWebSecurityIgnoringFilterConfig {


    @Bean
    public DynamicSecurityIgnoringFilterChainProxy.FilterChainProxyPostProcessor filterChainProxyPostProcessor() {
        return new DynamicSecurityIgnoringFilterChainProxy.FilterChainProxyPostProcessor();
    }

}
