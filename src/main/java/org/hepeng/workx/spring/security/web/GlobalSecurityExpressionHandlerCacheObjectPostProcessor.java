package org.hepeng.workx.spring.security.web;

import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.config.annotation.ObjectPostProcessor;

/**
 * @author he peng
 */
public class GlobalSecurityExpressionHandlerCacheObjectPostProcessor implements ObjectPostProcessor<SecurityExpressionHandler> {

    private static SecurityExpressionHandler securityExpressionHandler;

    @Override
    public <O extends SecurityExpressionHandler> O postProcess(O object) {
        securityExpressionHandler = object;
        return object;
    }

    public static SecurityExpressionHandler getSecurityExpressionHandler() {
        return securityExpressionHandler;
    }
}
