package org.hepeng.workx.spring.security.web.filter;

import org.hepeng.workx.spring.security.web.savedrequest.OriginalRequestUrlHttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author he peng
 */
public class OriginalRequestCacheAwareFilter extends GenericFilterBean {

    private RequestCache requestCache;

    public OriginalRequestCacheAwareFilter(String originalRequestUrlVarName) {
        this.requestCache = new OriginalRequestUrlHttpSessionRequestCache(originalRequestUrlVarName);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        requestCache.saveRequest((HttpServletRequest)request , (HttpServletResponse)response);
        chain.doFilter(request , response);
    }
}
