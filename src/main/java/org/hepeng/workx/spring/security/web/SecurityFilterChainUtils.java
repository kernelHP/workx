package org.hepeng.workx.spring.security.web;

import org.joor.Reflect;
import org.springframework.security.web.session.SessionManagementFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import java.util.ArrayList;
import java.util.List;

/**
 * @author he peng
 */
public class SecurityFilterChainUtils {

    public static void removeFilter(FilterChain filterChain , Class<? extends Filter> filterClass) {
        Reflect filterChainReflect = Reflect.on(filterChain);
        List<Filter> additionalFilters = filterChainReflect.get("additionalFilters");
        List<Filter> newFilters = new ArrayList<>();
        for (Filter filter : additionalFilters) {
            if (! filterClass.isAssignableFrom(filter.getClass())) {
                newFilters.add(filter);
            }
        }
        filterChainReflect.set("additionalFilters" , newFilters);
        filterChainReflect.set("size" , newFilters.size());
    }
}
