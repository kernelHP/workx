package org.hepeng.workx.spring.security.web.savedrequest;

import org.springframework.security.web.PortResolver;
import org.springframework.security.web.PortResolverImpl;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * @author he peng
 */
public class OriginalRequestUrlSavedRequest extends DefaultSavedRequest {

    private static final PortResolver DEFAULT_PORT_RESOLVER = new PortResolverImpl();

    private String originalRequestUrl;

    public OriginalRequestUrlSavedRequest(HttpServletRequest request) {
        super(request, DEFAULT_PORT_RESOLVER);
    }

    public String getOriginalRequestUrl() {
        return originalRequestUrl;
    }

    public void setOriginalRequestUrl(String originalRequestUrl) {
        this.originalRequestUrl = originalRequestUrl;
    }
}
