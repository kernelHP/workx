package org.hepeng.workx.spring.boot.autoconfigure.jdbc;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

import static org.hepeng.workx.spring.boot.autoconfigure.jdbc.MultipleDataSourceProperties.WORKX_MULTIPLE_DATA_SOURCE_PREFIX;

/**
 * @author he peng
 */

@ConfigurationProperties(prefix = WORKX_MULTIPLE_DATA_SOURCE_PREFIX)
@Data
public class MultipleDataSourceProperties {

    public static final String WORKX_MULTIPLE_DATA_SOURCE_PREFIX = "workx.spring";

    private Boolean enableMultipleDataSource;
    private Map<String , DataSourceOpsModeProperties> datasource;
}
