package org.hepeng.workx.spring.boot.autoconfigure.jdbc;

import lombok.Data;
import org.hepeng.workx.jdbc.DataSourceMetadata;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;

/**
 * @author he peng
 */

@Data
public class DataSourceOpsModeProperties extends DataSourceProperties {

    private DataSourceMetadata.DataSourceOpsMode mode;
}
