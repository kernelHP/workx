package org.hepeng.workx.spring.boot.env;

import lombok.Data;

/**
 * @author he peng
 */

@Data
public class EnvPropertySourceBean {

    private String key;

    private String val;

    private Class<? extends EnvSourceProcessor>[] processorClasses;
}
