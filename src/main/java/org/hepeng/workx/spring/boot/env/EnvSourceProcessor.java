package org.hepeng.workx.spring.boot.env;

/**
 * @author he peng
 */
public interface EnvSourceProcessor {

    Object process(String key , Object value);
}
