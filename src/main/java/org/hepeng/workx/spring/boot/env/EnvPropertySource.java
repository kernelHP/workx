package org.hepeng.workx.spring.boot.env;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author he peng
 */

@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnvPropertySource {

    String key();

    String val();

    Class<? extends EnvSourceProcessor>[] processors() default {};
}
