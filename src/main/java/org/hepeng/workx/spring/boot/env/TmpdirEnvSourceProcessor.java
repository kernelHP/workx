package org.hepeng.workx.spring.boot.env;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.io.File;

/**
 * @author he peng
 */
public class TmpdirEnvSourceProcessor implements EnvSourceProcessor {

    @Override
    public Object process(String key, Object tmpdir) {
        Assert.hasLength(key, "key must not be blank");
        Assert.isTrue(StringUtils.equals(key , "java.io.tmpdir") , "key must be equals java.io.tmpdir");
        Assert.notNull(tmpdir , "tmpdir must not be null");
        Assert.hasLength((String) tmpdir, "tmpdir must not be blank");

        File tmpdirFile = new File((String) tmpdir);
        if (! tmpdirFile.exists()) {
            boolean mkdirs = tmpdirFile.mkdirs();
            if (! mkdirs) {
                throw new IllegalStateException("mkdirs tmpdir [" + tmpdir + " ] failed");
            }
            System.setProperty(key , (String) tmpdir);
        } else {
            System.setProperty(key , (String) tmpdir);
        }

        return null;
    }
}
