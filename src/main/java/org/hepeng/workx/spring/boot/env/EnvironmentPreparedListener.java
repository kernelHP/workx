package org.hepeng.workx.spring.boot.env;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.PropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.env.SystemEnvironmentPropertySource;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author he peng
 */
public class EnvironmentPreparedListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

    private static final List<EnvPropertySourceBean> ENV_PROPERTY_SOURCE_BEANS = Collections.synchronizedList(new ArrayList<>());

    public EnvironmentPreparedListener(Class annotatedClass) {
        Assert.notNull(annotatedClass, "annotatedClass must not be null");
        EnvPropertySources envPropertySources = AnnotationUtils.findAnnotation(annotatedClass, EnvPropertySources.class);
        if (Objects.nonNull(envPropertySources)) {
            EnvPropertySource[] sources = envPropertySources.sources();
            if (ArrayUtils.isNotEmpty(sources)) {
                for (EnvPropertySource eps : sources) {
                    EnvPropertySourceBean epsBean = new EnvPropertySourceBean();
                    epsBean.setKey(eps.key());
                    epsBean.setVal(eps.val());
                    epsBean.setProcessorClasses(eps.processors());
                    ENV_PROPERTY_SOURCE_BEANS.add(epsBean);
                }
            }
        }
    }

    public EnvironmentPreparedListener addEnvPropertySource(EnvPropertySourceBean envPropertySourceBean) {
        Assert.notNull(envPropertySourceBean , "envPropertySourceBean must not be null");
        ENV_PROPERTY_SOURCE_BEANS.add(envPropertySourceBean);
        return this;
    }

    public EnvironmentPreparedListener addEnvPropertySource(List<EnvPropertySourceBean> envPropertySourceBeans) {
        Assert.notNull(envPropertySourceBeans , "envPropertySourceBeans must not be null");
        ENV_PROPERTY_SOURCE_BEANS.addAll(envPropertySourceBeans);
        return this;
    }

    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        if (CollectionUtils.isNotEmpty(ENV_PROPERTY_SOURCE_BEANS)) {
            Map<String, Object> source = new HashMap<>();
            PropertySource<?> propertySource = new SystemEnvironmentPropertySource("workxPropertySource" , source);
            StandardEnvironment parentEnv = new StandardEnvironment();
            parentEnv.getPropertySources().addFirst(propertySource);

            try {
                for (EnvPropertySourceBean eps : ENV_PROPERTY_SOURCE_BEANS) {
                    Map<String , Object> envSource = new HashMap<>();
                    envSource.put(eps.getKey() , eps.getVal());
                    source.putAll(envSource);
                    Class<? extends EnvSourceProcessor>[] processorClasses = eps.getProcessorClasses();
                    if (ArrayUtils.isNotEmpty(processorClasses)) {
                        for (Class<? extends EnvSourceProcessor> processorClass : processorClasses) {
                            EnvSourceProcessor envSourceProcessor = processorClass.newInstance();
                            envSourceProcessor.process(eps.getKey() , eps.getVal());
                        }
                    }
                }
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }

            event.getEnvironment().merge(parentEnv);
        }
    }
}
