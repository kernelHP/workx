package org.hepeng.workx.spring.cloud.netflix.zuul;

import com.netflix.zuul.IZuulFilter;
import org.hepeng.workx.extension.XLoader;
import org.hepeng.workx.spring.cloud.netflix.zuul.filter.RibbonRoutingFilterAopProxy;
import org.hepeng.workx.spring.cloud.netflix.zuul.filter.RibbonRoutingFilterProxy;
import org.hepeng.workx.util.proxy.ProxyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.netflix.ribbon.support.RibbonRequestCustomizer;
import org.springframework.cloud.netflix.zuul.ZuulProxyAutoConfiguration;
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper;
import org.springframework.cloud.netflix.zuul.filters.route.RibbonCommandFactory;
import org.springframework.cloud.netflix.zuul.filters.route.RibbonRoutingFilter;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author he peng
 */

@ConditionalOnClass({IZuulFilter.class, ZuulProxyAutoConfiguration.class})
public class ZuulRibbonRoutingFilterProxyConfiguration {

    private static final ProxyFactory PROXY_FACTORY = XLoader.getXLoader(ProxyFactory.class).getX("cglib");

    @Autowired(required = false)
    private List<RibbonRequestCustomizer> requestCustomizers = Collections.emptyList();


//    @Bean
    public RibbonRoutingFilterAopProxy ribbonRoutingFilterAopProxy() {
        return new RibbonRoutingFilterAopProxy();
    }

}
