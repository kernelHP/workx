package org.hepeng.workx.spring.cloud.netflix.ribbon.loadbalancer;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.Server;
import org.apache.commons.lang3.StringUtils;
import org.hepeng.workx.spring.cloud.netflix.ribbon.RibbonRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author he peng
 */
public class ServerHostRule extends AbstractThreadIsolationRule {

    private static final Logger LOG = LoggerFactory.getLogger(ServerHostRule.class);

    public ServerHostRule() {}

    public ServerHostRule(String serverHostParamName) {
        super(serverHostParamName);
    }

    public ServerHostRule(String serverHostParamName, IRule rule) {
        super(serverHostParamName, rule);
    }

    @Override
    public List<Server> getParticipateLoadBalancingServers(Object key, RibbonRequestContext ribbonContext, List<Server> originalAllServers) {
        LOG.info("{} rules works" , ServerHostRule.class.getName());
        List<Server> loadBalancingServers = new ArrayList<>();
        String host = getDecideLoadBalanceParam(ribbonContext);

        LOG.info("The specified routing host is {} " , host);
        for (Server server : originalAllServers) {
            String serverHost = server.getHost();
            if (StringUtils.equals(host , serverHost)) {
                LOG.info("Matched server {} " , server);
                loadBalancingServers.add(server);
            }
        }
        return loadBalancingServers;
    }
}
