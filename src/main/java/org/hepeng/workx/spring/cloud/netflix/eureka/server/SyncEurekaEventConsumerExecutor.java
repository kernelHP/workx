package org.hepeng.workx.spring.cloud.netflix.eureka.server;

import io.reactivex.Observable;
import org.springframework.context.ApplicationEvent;

import java.util.List;

/**
 * @author he peng
 */
public class SyncEurekaEventConsumerExecutor implements EurekaEventConsumerExecutor {

    @Override
    public void execute(ApplicationEvent event, List<EurekaServerEventConsumer> consumers) {
        Observable<ApplicationEvent> observable = Observable.just(event);
        consumers.forEach(
                consumer -> observable.subscribe(
                        data -> consumer.onConsume(data) ,
                        error -> consumer.onError(error),
                        () -> consumer.onComplete()));
    }
}
