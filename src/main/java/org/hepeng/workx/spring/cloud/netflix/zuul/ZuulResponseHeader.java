package org.hepeng.workx.spring.cloud.netflix.zuul;

import org.hepeng.workx.spring.cloud.netflix.zuul.filter.ZuulResponseWriterHeaderFilter;
import org.hepeng.workx.spring.cloud.netflix.zuul.filter.ZuulResponseHeaderConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author he peng
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(ZuulResponseHeaderConfiguration.class)
public @interface ZuulResponseHeader {

    Class<? extends ZuulResponseWriterHeaderFilter.HeaderWriter>[] headerWriters() default {};
}
