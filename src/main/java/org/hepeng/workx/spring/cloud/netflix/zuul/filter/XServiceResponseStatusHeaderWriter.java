package org.hepeng.workx.spring.cloud.netflix.zuul.filter;

import com.netflix.zuul.context.RequestContext;

/**
 * @author he peng
 */
public class XServiceResponseStatusHeaderWriter implements ZuulResponseWriterHeaderFilter.HeaderWriter {

    public static final String X_SERVICE_RESPONSE_STATUS_HEADER = "X-Service-Response-Status";

    @Override
    public void writeHeaders(RequestContext requestContext) {
        requestContext.getResponse().setHeader(X_SERVICE_RESPONSE_STATUS_HEADER , String.valueOf(requestContext.getResponseStatusCode()));
    }
}
