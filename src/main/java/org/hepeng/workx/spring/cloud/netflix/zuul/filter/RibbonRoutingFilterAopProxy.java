package org.hepeng.workx.spring.cloud.netflix.zuul.filter;

import org.apache.commons.lang3.ArrayUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.hepeng.workx.spring.cloud.netflix.ribbon.RibbonRequestContextHolder;

/**
 * @author he peng
 */

@Aspect
public class RibbonRoutingFilterAopProxy {


    @Around("execution(* org.springframework.cloud.netflix.zuul.filters.route.RibbonRoutingFilter.*(..))")
    public Object runMethodProxy(ProceedingJoinPoint pjp) throws Throwable {

        Object result;
        try {

            Object target = pjp.getTarget();
            String name = pjp.getSignature().getName();
//            Object commandContext = Reflect.on(target).call("buildCommandContext" , RequestContext.getCurrentContext()).get();

            Object[] args = pjp.getArgs();
            if (ArrayUtils.isNotEmpty(args)) {
                result = pjp.proceed(pjp.getArgs());
            } else {
               result = pjp.proceed();
            }
//            RibbonRequestContext safeRibbonCommandContext = RibbonRequestContext.copy(pjp.getArgs()[0]);
//            RibbonRequestContextHolder.set(safeRibbonCommandContext);
        } finally {
            RibbonRequestContextHolder.clear();
        }
        return result;
    }
}
