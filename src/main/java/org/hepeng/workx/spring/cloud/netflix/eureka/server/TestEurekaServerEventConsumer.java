package org.hepeng.workx.spring.cloud.netflix.eureka.server;

import org.springframework.cloud.netflix.eureka.server.event.EurekaServerStartedEvent;

/**
 * @author he peng
 */
public class TestEurekaServerEventConsumer implements EurekaServerEventConsumer<EurekaServerStartedEvent> {

    @Override
    public void onConsume(EurekaServerStartedEvent eurekaServerStartedEvent) {
        System.out.println("Thread ->" + Thread.currentThread().getName() +  " eureka server started -> " + eurekaServerStartedEvent);
    }

    @Override
    public void onError(Throwable error) {
        System.err.println("mybatis eureka server event consumer on error -> " + error);
    }

    @Override
    public void onComplete() {
        System.out.println("mybatis eureka server event consumer on complete ->");
    }
}
