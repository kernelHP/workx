package org.hepeng.workx.spring.cloud.netflix.eureka.server;

import org.springframework.cloud.netflix.eureka.server.event.EurekaInstanceCanceledEvent;
import org.springframework.cloud.netflix.eureka.server.event.EurekaInstanceRegisteredEvent;
import org.springframework.cloud.netflix.eureka.server.event.EurekaInstanceRenewedEvent;
import org.springframework.cloud.netflix.eureka.server.event.EurekaRegistryAvailableEvent;
import org.springframework.cloud.netflix.eureka.server.event.EurekaServerStartedEvent;
import org.springframework.context.ApplicationEvent;

/**
 * @author he peng
 */
public enum EurekaEvent {

    CANCELED(EurekaInstanceCanceledEvent.class),

    REGISTERED(EurekaInstanceRegisteredEvent.class),

    RENEWED(EurekaInstanceRenewedEvent.class),

    EUREKA_SERVER_AVAILABLE(EurekaRegistryAvailableEvent.class),

    EUREKA_SERVER_STARTED(EurekaServerStartedEvent.class);

    private Class<? extends ApplicationEvent> eventClass;

    EurekaEvent(Class<? extends ApplicationEvent> eventClass) {
        this.eventClass = eventClass;
    }

}
