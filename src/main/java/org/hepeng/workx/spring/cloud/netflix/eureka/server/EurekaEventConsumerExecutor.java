package org.hepeng.workx.spring.cloud.netflix.eureka.server;

import org.springframework.context.ApplicationEvent;

import java.util.List;

/**
 * @author he peng
 */
public interface EurekaEventConsumerExecutor {

    void execute(ApplicationEvent event , List<EurekaServerEventConsumer> consumers);
}
