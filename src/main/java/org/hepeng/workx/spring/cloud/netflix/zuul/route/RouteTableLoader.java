package org.hepeng.workx.spring.cloud.netflix.zuul.route;

import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;

import java.util.Map;
import java.util.Set;

/**
 * @author he peng
 */
public interface RouteTableLoader<T> {

    Set<T> loadRouteTable();

    Map<String, ZuulProperties.ZuulRoute> convert(Set<T> routes);
}
