package org.hepeng.workx.spring.cloud.feign.hystrix.fallback;

import feign.hystrix.FallbackFactory;


/**
 * @author he peng
 */
public class ReturnNullFallbackFactory implements FallbackFactory<Object> {

    @Override
    public Object create(Throwable cause) {
        return null;
    }
}
