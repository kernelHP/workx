package org.hepeng.workx.spring.cloud.netflix.ribbon;

/**
 * @author he peng
 */
public class RibbonRequestContextHolder {

    private static final ThreadLocal<RibbonRequestContext> THREAD_LOCAL = new ThreadLocal<>();

    public static void set(RibbonRequestContext context) {
        THREAD_LOCAL.set(context);
    }

    public static RibbonRequestContext get() {
        return THREAD_LOCAL.get();
    }


    public static void clear() {
        THREAD_LOCAL.remove();
    }

}
