package org.hepeng.workx.spring.cloud.netflix.ribbon;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.joor.Reflect;
import org.springframework.cloud.netflix.ribbon.support.RibbonRequestCustomizer;
import org.springframework.util.MultiValueMap;

import java.io.InputStream;
import java.util.List;

/**
 * @author he peng
 */

@Builder
@AllArgsConstructor
@Data
public class RibbonRequestContext {

    private String serviceId;
    private String method;
    private String uri;
    private Boolean retryable;
    private MultiValueMap<String, String> headers;
    private MultiValueMap<String, String> params;
    private List<RibbonRequestCustomizer> requestCustomizers;
    private InputStream requestEntity;
    private Long contentLength;
    private Object loadBalancerKey;

    public static RibbonRequestContext copy(Object ribbonContext) {
        Reflect reflect = Reflect.on(ribbonContext);
        RibbonRequestContext context = RibbonRequestContext.builder()
                .serviceId(reflect.get("serviceId"))
                .method(reflect.get("method"))
                .uri(reflect.get("uri"))
                .retryable(reflect.get("retryable"))
                .headers(reflect.get("headers"))
                .params(reflect.get("params"))
                .requestCustomizers(reflect.get("requestCustomizers"))
                .requestEntity(reflect.get("requestEntity"))
                .contentLength(reflect.get("contentLength"))
                .loadBalancerKey(reflect.get("loadBalancerKey"))
                .build();
        return context;
    }
}
