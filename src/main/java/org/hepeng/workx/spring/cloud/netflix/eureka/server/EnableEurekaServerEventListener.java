package org.hepeng.workx.spring.cloud.netflix.eureka.server;

import org.hepeng.workx.constant.ExecuteMode;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author he peng
 */


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(EurekaServerEventListenerConfiguration.class)
public @interface EnableEurekaServerEventListener {

    ExecuteMode executeMode() default ExecuteMode.SYNC;
}
