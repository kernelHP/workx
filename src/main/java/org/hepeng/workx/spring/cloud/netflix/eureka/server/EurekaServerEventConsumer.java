package org.hepeng.workx.spring.cloud.netflix.eureka.server;


/**
 * @author he peng
 */
public interface EurekaServerEventConsumer<Event> {

    void onConsume(Event event);

    void onError(Throwable error);

    void onComplete();
}
