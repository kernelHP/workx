package org.hepeng.workx.spring.cloud.netflix.zuul;

import org.hepeng.workx.exception.ApplicationRuntimeException;
import org.springframework.cloud.netflix.zuul.ZuulProxyAutoConfiguration;
import org.springframework.cloud.netflix.zuul.web.ZuulController;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author he peng
 */

public class AsyncZuulProxyAutoConfiguration extends ZuulProxyAutoConfiguration {

    @Bean
    public AsyncTaskExecutor zuulAsyncTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("zuul-async-");
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(60);
        executor.initialize();
        return executor;
    }

    @Override
    public final ZuulController zuulController() {
        AsyncZuulController asyncZuulController = new AsyncZuulController(zuulAsyncTaskExecutor());
        try {
            asyncZuulController.afterPropertiesSet();
        } catch (Exception e) {
            throw new ApplicationRuntimeException(AsyncZuulController.class + " init error" , e);
        }
        return asyncZuulController;
    }
}
