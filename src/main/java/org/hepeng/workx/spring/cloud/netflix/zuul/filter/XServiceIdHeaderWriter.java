package org.hepeng.workx.spring.cloud.netflix.zuul.filter;

import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import java.util.Objects;

/**
 * @author he peng
 */
public class XServiceIdHeaderWriter implements ZuulResponseWriterHeaderFilter.HeaderWriter {

    public static final String X_SERVICE_ID_HEADER = "X-Service-ID";

    @Override
    public void writeHeaders(RequestContext requestContext) {
        Object obj = requestContext.get(FilterConstants.SERVICE_ID_KEY);
        String serviceId = Objects.nonNull(obj) ? (String) obj : "unknown";
        requestContext.getResponse().setHeader(X_SERVICE_ID_HEADER , serviceId);

    }
}
