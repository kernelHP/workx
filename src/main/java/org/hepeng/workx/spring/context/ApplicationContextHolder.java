package org.hepeng.workx.spring.context;

import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.Assert;

/**
 * @author he peng
 */

@ConditionalOnClass(ApplicationContext.class)
public class ApplicationContextHolder implements ApplicationContextAware {

    private static ApplicationContext APPLICATION_CONTEXT;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        APPLICATION_CONTEXT = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        Assert.notNull(APPLICATION_CONTEXT , ApplicationContextHolder.class.getName() + " not initialized by spring");
        return APPLICATION_CONTEXT;
    }

}
