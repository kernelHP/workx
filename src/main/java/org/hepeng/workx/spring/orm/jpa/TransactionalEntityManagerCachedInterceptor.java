package org.hepeng.workx.spring.orm.jpa;

import org.hepeng.workx.spring.orm.TXSyncResourceHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.AsyncWebRequestInterceptor;
import org.springframework.web.context.request.WebRequest;


/**
 * @author he peng
 */
public class TransactionalEntityManagerCachedInterceptor implements AsyncWebRequestInterceptor {

    @Override
    public void afterConcurrentHandlingStarted(WebRequest request) {

    }

    @Override
    public void preHandle(WebRequest request) throws Exception {
        TXSyncResourceHolder.set(TransactionSynchronizationManager.getResourceMap());
    }

    @Override
    public void postHandle(WebRequest request, ModelMap model) throws Exception {
        TXSyncResourceHolder.clear();
    }

    @Override
    public void afterCompletion(WebRequest request, Exception ex) throws Exception {

    }
}
