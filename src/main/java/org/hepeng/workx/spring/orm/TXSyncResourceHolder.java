package org.hepeng.workx.spring.orm;

import com.alibaba.ttl.TransmittableThreadLocal;

import java.util.Map;

/**
 * @author he peng
 */
public class TXSyncResourceHolder {

    private static final ThreadLocal<Map<Object , Object>> HOLDER = new TransmittableThreadLocal<>();

    public static void set(Map<Object , Object> resources) {
        HOLDER.set(resources);
    }

    public static Map<Object , Object> get() {
        return HOLDER.get();
    }

    public static Map<Object , Object> clear() {
        Map<Object, Object> resource = HOLDER.get();
        HOLDER.remove();
        return resource;
    }
}
