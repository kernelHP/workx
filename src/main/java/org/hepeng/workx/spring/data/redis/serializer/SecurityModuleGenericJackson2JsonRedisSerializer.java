package org.hepeng.workx.spring.data.redis.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joor.Reflect;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.security.jackson2.CoreJackson2Module;
import org.springframework.security.web.jackson2.WebJackson2Module;

/**
 * https://github.com/spring-projects/spring-session/issues/686
 * https://github.com/spring-projects/spring-security/issues/3736
 * https://github.com/spring-projects/spring-security/issues/4370
 *
 * @author he peng
 */
public class SecurityModuleGenericJackson2JsonRedisSerializer extends GenericJackson2JsonRedisSerializer {

    public SecurityModuleGenericJackson2JsonRedisSerializer() {
        this((String) null);
    }

    public SecurityModuleGenericJackson2JsonRedisSerializer(String classPropertyTypeName) {
        super(classPropertyTypeName);
        registerSecurityModule(Reflect.on(this).get("mapper"));
    }

    public SecurityModuleGenericJackson2JsonRedisSerializer(ObjectMapper mapper) {
        super(mapper);
        registerSecurityModule(mapper);
    }

    private void registerSecurityModule(ObjectMapper mapper) {
        mapper.registerModule(new CoreJackson2Module());
        mapper.registerModule(new WebJackson2Module());
    }
}
