package org.hepeng.workx.exception;

/**
 * @author he peng
 */
public class NoSuchConstructorException extends RuntimeException {

    public NoSuchConstructorException(String message) {
        super(message);
    }
}
