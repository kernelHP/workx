package org.hepeng.workx.exception;

import org.hepeng.workx.service.error.Error;

/**
 * 服务异常
 * @author he peng
 */
public class ServiceException extends ApplicationRuntimeException {

    private Error error;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Error error) {
        this(error.getMsg());
        this.error = error;
    }

    public ServiceException(String message , Error error) {
        super(message);
        this.error = error;
    }

    public ServiceException(Throwable cause , Error error) {
        super(cause);
        this.error = error;
    }

    public ServiceException(String message, Throwable cause , Error error) {
        super(message, cause);
        this.error = error;
    }

    public Error getError() {
        return error;
    }

    @Override
    public String toString() {
        return "ServiceException{" +
                "error=" + error +
                '}';
    }
}
