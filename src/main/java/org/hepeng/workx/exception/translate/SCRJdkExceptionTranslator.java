package org.hepeng.workx.exception.translate;

import org.hepeng.workx.service.error.Error;
import org.hepeng.workx.service.error.ServerError;
import org.hepeng.workx.service.ServiceCallResult;

/**
 * @author he peng
 */
public class SCRJdkExceptionTranslator extends AbstractServiceCallResultExceptionTranslator {

    @Override
    public ServiceCallResult<? extends Error, Object> translate(Throwable t) {
        return serverError();
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(Exception ex) {
        return serverError();
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(RuntimeException ex) {
        return serverError();
    }

    private ServiceCallResult<? extends Error, Object> serverError() {
        return builder().error(ServerError.SERVER_ERROR).build();
    }
}
