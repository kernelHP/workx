package org.hepeng.workx.exception.translate;

import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.IncorrectResultSetColumnCountException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.JdbcUpdateAffectedIncorrectNumberOfRowsException;
import org.springframework.jdbc.LobRetrievalFailureException;
import org.springframework.jdbc.SQLWarningException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author he peng
 */
public interface SpringJDBCExceptionTranslator<T> extends SpringDAOExceptionTranslator<T> {

    @ExceptionHandler(BadSqlGrammarException.class)
    T translate(BadSqlGrammarException ex);

    @ExceptionHandler(CannotGetJdbcConnectionException.class)
    T translate(CannotGetJdbcConnectionException ex);

    @ExceptionHandler(IncorrectResultSetColumnCountException.class)
    T translate(IncorrectResultSetColumnCountException ex);

    @ExceptionHandler(InvalidResultSetAccessException.class)
    T translate(InvalidResultSetAccessException ex);

    @ExceptionHandler(JdbcUpdateAffectedIncorrectNumberOfRowsException.class)
    T translate(JdbcUpdateAffectedIncorrectNumberOfRowsException ex);

    @ExceptionHandler(LobRetrievalFailureException.class)
    T translate(LobRetrievalFailureException ex);

    @ExceptionHandler(SQLWarningException.class)
    T translate(SQLWarningException ex);

    @ExceptionHandler(UncategorizedSQLException.class)
    T translate(UncategorizedSQLException ex);
}
