package org.hepeng.workx.exception.translate;

import org.apache.ibatis.cache.CacheException;
import org.apache.ibatis.datasource.DataSourceException;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.apache.ibatis.executor.BatchExecutorException;
import org.apache.ibatis.executor.ExecutorException;
import org.apache.ibatis.executor.result.ResultMapException;
import org.apache.ibatis.javassist.CannotCompileException;
import org.apache.ibatis.javassist.NotFoundException;
import org.apache.ibatis.jdbc.RuntimeSqlException;
import org.apache.ibatis.logging.LogException;
import org.apache.ibatis.ognl.ExpressionSyntaxException;
import org.apache.ibatis.ognl.InappropriateExpressionException;
import org.apache.ibatis.ognl.MethodFailedException;
import org.apache.ibatis.ognl.NoSuchPropertyException;
import org.apache.ibatis.ognl.OgnlException;
import org.apache.ibatis.ognl.enhance.UnsupportedCompilationException;
import org.apache.ibatis.parsing.ParsingException;
import org.apache.ibatis.plugin.PluginException;
import org.apache.ibatis.reflection.ReflectionException;
import org.apache.ibatis.scripting.ScriptingException;
import org.apache.ibatis.session.SqlSessionException;
import org.apache.ibatis.transaction.TransactionException;
import org.apache.ibatis.type.TypeException;
import org.hepeng.workx.service.error.CacheError;
import org.hepeng.workx.service.error.Error;
import org.hepeng.workx.service.error.JDBCError;
import org.hepeng.workx.service.ServiceCallResult;

/**
 * @author he peng
 */
public class SCRMyBatisExceptionTranslator extends SCRJdkExceptionTranslator
        implements MyBatisExceptionTranslator<ServiceCallResult<? extends Error, Object>> {

    @Override
    public ServiceCallResult<? extends Error, Object> translate(CacheException ex) {
        return build(CacheError.CACHE_ERROR, getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(DataSourceException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(PersistenceException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(TooManyResultsException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(ResultMapException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(BatchExecutorException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(ExecutorException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(CannotCompileException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(NotFoundException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(RuntimeSqlException ex) {
        return build(JDBCError.SQL_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(LogException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(UnsupportedCompilationException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(ExpressionSyntaxException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(InappropriateExpressionException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(MethodFailedException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(NoSuchPropertyException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(OgnlException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(ParsingException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(PluginException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(ReflectionException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(ScriptingException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(SqlSessionException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(TransactionException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(TypeException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

}
