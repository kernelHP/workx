package org.hepeng.workx.exception.translate;

import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author he peng
 */
public interface JdkExceptionTranslator<T> extends ExceptionTranslator<T> {

    @ExceptionHandler(Throwable.class)
    T translate(Throwable t);

    @ExceptionHandler(Exception.class)
    T translate(Exception ex);

    @ExceptionHandler(RuntimeException.class)
    T translate(RuntimeException ex);
}
