package org.hepeng.workx.exception.translate;

import org.hepeng.workx.service.CommonServiceCallResult;
import org.hepeng.workx.service.error.Error;
import org.hepeng.workx.service.ServiceCallResult;
import org.hepeng.workx.util.proxy.NativeInvoke;

/**
 * @author he peng
 */
public abstract class AbstractServiceCallResultExceptionTranslator implements JdkExceptionTranslator<ServiceCallResult<? extends Error, Object>> {

    @NativeInvoke
    protected CommonServiceCallResult.ServiceCallResultBuilder builder() {
        return new CommonServiceCallResult.ServiceCallResultBuilder();
    }

    @NativeInvoke
    protected ServiceCallResult build(Error error , String msg) {
        return builder().error(error).msg(spliceWithRootCauseMsg(error.getMsg() , msg)).build();
    }
}
