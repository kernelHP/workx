package org.hepeng.workx.exception.translate;

import org.hepeng.workx.service.ServiceCallResult;
import org.hepeng.workx.service.error.Error;
import org.hepeng.workx.service.error.JDBCError;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.IncorrectResultSetColumnCountException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.JdbcUpdateAffectedIncorrectNumberOfRowsException;
import org.springframework.jdbc.LobRetrievalFailureException;
import org.springframework.jdbc.SQLWarningException;
import org.springframework.jdbc.UncategorizedSQLException;

/**
 * @author he peng
 */
public class SCRSpringJDBCExceptionTranslator extends SCRSpringDAOExceptionTranslator
        implements SpringJDBCExceptionTranslator<ServiceCallResult<? extends Error, Object>> {

    @Override
    public ServiceCallResult<? extends Error, Object> translate(BadSqlGrammarException ex) {
        return build(JDBCError.SQL_ERROR , spliceWithRootCauseMsg("SQL is invalid" , ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(CannotGetJdbcConnectionException ex) {
        return build(JDBCError.CONNECTION_ERROR ,
                spliceWithRootCauseMsg("can't connect to an RDBMS using JDBC" , ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(IncorrectResultSetColumnCountException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(InvalidResultSetAccessException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(JdbcUpdateAffectedIncorrectNumberOfRowsException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(LobRetrievalFailureException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(SQLWarningException ex) {
        return build(JDBCError.SQL_ERROR , getRootCauseMsg(ex));
    }

    @Override
    public ServiceCallResult<? extends Error, Object> translate(UncategorizedSQLException ex) {
        return build(JDBCError.JDBC_ERROR , getRootCauseMsg(ex));
    }
}
