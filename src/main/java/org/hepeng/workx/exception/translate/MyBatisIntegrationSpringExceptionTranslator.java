package org.hepeng.workx.exception.translate;

import org.mybatis.spring.MyBatisSystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author he peng
 */
public interface MyBatisIntegrationSpringExceptionTranslator<T> {

    @ExceptionHandler(MyBatisSystemException.class)
    T translate(MyBatisSystemException ex);
}
