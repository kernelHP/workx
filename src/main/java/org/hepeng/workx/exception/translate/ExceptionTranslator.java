package org.hepeng.workx.exception.translate;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hepeng.workx.util.proxy.NativeInvoke;

/**
 * @author he peng
 */
public interface ExceptionTranslator<T> {

    @NativeInvoke
    default String getRootCauseMsg(Throwable t) {
        return ExceptionUtils.getRootCauseMessage(t);

    }

    @NativeInvoke
    default String spliceWithRootCauseMsg(String msg , Throwable t) {
        return msg + " : " + getRootCauseMsg(t);
    }

    @NativeInvoke
    default String spliceWithRootCauseMsg(String msg , String causeMsg) {
        return msg + " : " + causeMsg;
    }

    @NativeInvoke
    default Throwable getRootCause(Throwable t) {
        return ExceptionUtils.getRootCause(t);
    }
}
