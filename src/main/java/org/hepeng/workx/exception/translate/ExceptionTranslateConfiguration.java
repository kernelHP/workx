package org.hepeng.workx.exception.translate;

import org.hepeng.workx.extension.XLoader;
import org.hepeng.workx.util.proxy.Invoker;
import org.hepeng.workx.util.proxy.ProxyFactory;
import org.springframework.context.annotation.Bean;

import java.util.LinkedList;
import java.util.List;

/**
 * @author he peng
 */
public class ExceptionTranslateConfiguration {

    @Bean
    public ExceptionTranslateAnnotationBeanPostProcessor exceptionTranslateAnnotationBeanPostProcessor() {
        return new ExceptionTranslateAnnotationBeanPostProcessor();
    }

    @Bean
    public RSCRSpringWebLayerExceptionTranslator webLayerExceptionTranslator() {
        ProxyFactory proxyFactory = XLoader.getXLoader(ProxyFactory.class).getX();
        RSCRSpringWebLayerExceptionTranslator webLayerExceptionTranslator =
                new RSCRSpringWebLayerExceptionTranslator();
        List<Invoker> invokers = new LinkedList<>();
        invokers.add(webLayerExceptionTranslator);
        Object proxy = proxyFactory.createProxy(webLayerExceptionTranslator, null , invokers , null);
        return (RSCRSpringWebLayerExceptionTranslator) proxy;
    }
}
