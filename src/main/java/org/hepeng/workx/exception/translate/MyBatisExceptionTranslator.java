package org.hepeng.workx.exception.translate;

import org.apache.ibatis.cache.CacheException;
import org.apache.ibatis.datasource.DataSourceException;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.apache.ibatis.executor.BatchExecutorException;
import org.apache.ibatis.executor.ExecutorException;
import org.apache.ibatis.executor.result.ResultMapException;
import org.apache.ibatis.javassist.CannotCompileException;
import org.apache.ibatis.javassist.NotFoundException;
import org.apache.ibatis.jdbc.RuntimeSqlException;
import org.apache.ibatis.logging.LogException;
import org.apache.ibatis.ognl.ExpressionSyntaxException;
import org.apache.ibatis.ognl.InappropriateExpressionException;
import org.apache.ibatis.ognl.MethodFailedException;
import org.apache.ibatis.ognl.NoSuchPropertyException;
import org.apache.ibatis.ognl.OgnlException;
import org.apache.ibatis.ognl.enhance.UnsupportedCompilationException;
import org.apache.ibatis.parsing.ParsingException;
import org.apache.ibatis.plugin.PluginException;
import org.apache.ibatis.reflection.ReflectionException;
import org.apache.ibatis.scripting.ScriptingException;
import org.apache.ibatis.session.SqlSessionException;
import org.apache.ibatis.transaction.TransactionException;
import org.apache.ibatis.type.TypeException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author he peng
 */
public interface MyBatisExceptionTranslator<T> {

    @ExceptionHandler(CacheException.class)
    T translate(CacheException ex);

    @ExceptionHandler(DataSourceException.class)
    T translate(DataSourceException ex);

    @ExceptionHandler(PersistenceException.class)
    T translate(PersistenceException ex);

    @ExceptionHandler(TooManyResultsException.class)
    T translate(TooManyResultsException ex);

    @ExceptionHandler(ResultMapException.class)
    T translate(ResultMapException ex);

    @ExceptionHandler(BatchExecutorException.class)
    T translate(BatchExecutorException ex);

    @ExceptionHandler(ExecutorException.class)
    T translate(ExecutorException ex);

    @ExceptionHandler(CannotCompileException.class)
    T translate(CannotCompileException ex);

    @ExceptionHandler(NotFoundException.class)
    T translate(NotFoundException ex);

    @ExceptionHandler(RuntimeSqlException.class)
    T translate(RuntimeSqlException ex);

    @ExceptionHandler(LogException.class)
    T translate(LogException ex);

    @ExceptionHandler(UnsupportedCompilationException.class)
    T translate(UnsupportedCompilationException ex);

    @ExceptionHandler(ExpressionSyntaxException.class)
    T translate(ExpressionSyntaxException ex);

    @ExceptionHandler(InappropriateExpressionException.class)
    T translate(InappropriateExpressionException ex);

    @ExceptionHandler(MethodFailedException.class)
    T translate(MethodFailedException ex);

    @ExceptionHandler(NoSuchPropertyException.class)
    T translate(NoSuchPropertyException ex);

    @ExceptionHandler(OgnlException.class)
    T translate(OgnlException ex);

    @ExceptionHandler(ParsingException.class)
    T translate(ParsingException ex);

    @ExceptionHandler(PluginException.class)
    T translate(PluginException ex);

    @ExceptionHandler(ReflectionException.class)
    T translate(ReflectionException ex);

    @ExceptionHandler(ScriptingException.class)
    T translate(ScriptingException ex);

    @ExceptionHandler(SqlSessionException.class)
    T translate(SqlSessionException ex);

    @ExceptionHandler(TransactionException.class)
    T translate(TransactionException ex);

    @ExceptionHandler(TypeException.class)
    T translate(TypeException ex);
}
