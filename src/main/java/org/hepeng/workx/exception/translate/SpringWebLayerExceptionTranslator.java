package org.hepeng.workx.exception.translate;

import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.server.MediaTypeNotSupportedStatusException;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.server.NotAcceptableStatusException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerErrorException;
import org.springframework.web.server.ServerWebInputException;
import org.springframework.web.server.UnsupportedMediaTypeStatusException;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * @author he peng
 */
public interface SpringWebLayerExceptionTranslator<T> {

    @ExceptionHandler(HttpMessageConversionException.class)
    T translate(HttpMessageConversionException ex);

    @ExceptionHandler(HttpMessageNotReadableException.class)
    T translate(HttpMessageNotReadableException ex);

    @ExceptionHandler(HttpMessageNotWritableException.class)
    T translate(HttpMessageNotWritableException ex);

    @ExceptionHandler(InvalidMediaTypeException.class)
    T translate(InvalidMediaTypeException ex);

    @ExceptionHandler(MethodArgumentNotValidException.class)
    T translate(MethodArgumentNotValidException ex);

    @ExceptionHandler(MissingPathVariableException.class)
    T translate(MissingPathVariableException ex);

    @ExceptionHandler(MissingServletRequestParameterException.class)
    T translate(MissingServletRequestParameterException ex);

    @ExceptionHandler(ServletRequestBindingException.class)
    T translate(ServletRequestBindingException ex);

    @ExceptionHandler(UnsatisfiedServletRequestParameterException.class)
    T translate(UnsatisfiedServletRequestParameterException ex);

    @ExceptionHandler(HttpClientErrorException.class)
    T translate(HttpClientErrorException ex);

    @ExceptionHandler(HttpServerErrorException.class)
    T translate(HttpServerErrorException ex);

    @ExceptionHandler(HttpStatusCodeException.class)
    T translate(HttpStatusCodeException ex);

    @ExceptionHandler(ResourceAccessException.class)
    T translate(ResourceAccessException ex);

    @ExceptionHandler(RestClientException.class)
    T translate(RestClientException ex);

    @ExceptionHandler(RestClientResponseException.class)
    T translate(RestClientResponseException ex);

    @ExceptionHandler(UnknownHttpStatusCodeException.class)
    T translate(UnknownHttpStatusCodeException ex);

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    T translate(MaxUploadSizeExceededException ex);

    @ExceptionHandler(MultipartException.class)
    T translate(MultipartException ex);

    @ExceptionHandler(MediaTypeNotSupportedStatusException.class)
    T translate(MediaTypeNotSupportedStatusException ex);

    @ExceptionHandler(MethodNotAllowedException.class)
    T translate(MethodNotAllowedException ex);

    @ExceptionHandler(NotAcceptableStatusException.class)
    T translate(NotAcceptableStatusException ex);

    @ExceptionHandler(ServerErrorException.class)
    T translate(ServerErrorException ex);

    @ExceptionHandler(ServerWebInputException.class)
    T translate(ServerWebInputException ex);

    @ExceptionHandler(UnsupportedMediaTypeStatusException.class)
    T translate(UnsupportedMediaTypeStatusException ex);

    @ExceptionHandler(HttpMediaTypeException.class)
    T translate(HttpMediaTypeException ex);

    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    T translate(HttpMediaTypeNotAcceptableException ex);

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    T translate(HttpMediaTypeNotSupportedException ex);

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    T translate(HttpRequestMethodNotSupportedException ex);

    @ExceptionHandler(HttpSessionRequiredException.class)
    T translate(HttpSessionRequiredException ex);

    @ExceptionHandler(ModelAndViewDefiningException.class)
    T translate(ModelAndViewDefiningException ex);

    @ExceptionHandler(NoHandlerFoundException.class)
    T translate(NoHandlerFoundException ex);

    @ExceptionHandler(ResponseStatusException.class)
    T translate(ResponseStatusException ex);
}


