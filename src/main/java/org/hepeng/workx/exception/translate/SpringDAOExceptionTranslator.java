package org.hepeng.workx.exception.translate;

import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.CannotSerializeTransactionException;
import org.springframework.dao.CleanupFailureDataAccessException;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.DeadlockLoserDataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.dao.NonTransientDataAccessException;
import org.springframework.dao.NonTransientDataAccessResourceException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.dao.QueryTimeoutException;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.dao.TransientDataAccessResourceException;
import org.springframework.dao.TypeMismatchDataAccessException;
import org.springframework.dao.UncategorizedDataAccessException;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.HeuristicCompletionException;
import org.springframework.transaction.IllegalTransactionStateException;
import org.springframework.transaction.InvalidIsolationLevelException;
import org.springframework.transaction.InvalidTimeoutException;
import org.springframework.transaction.NestedTransactionNotSupportedException;
import org.springframework.transaction.NoTransactionException;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionSuspensionNotSupportedException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.TransactionTimedOutException;
import org.springframework.transaction.TransactionUsageException;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author he peng
 */
public interface SpringDAOExceptionTranslator<T> extends JdkExceptionTranslator<T> {

    @ExceptionHandler(CannotAcquireLockException.class)
    T translate(CannotAcquireLockException ex);

    @ExceptionHandler(CannotSerializeTransactionException.class)
    T translate(CannotSerializeTransactionException ex);

    @ExceptionHandler(CleanupFailureDataAccessException.class)
    T translate(CleanupFailureDataAccessException ex);

    @ExceptionHandler(ConcurrencyFailureException.class)
    T translate(ConcurrencyFailureException ex);

    @ExceptionHandler(DataAccessException.class)
    T translate(DataAccessException ex);

    @ExceptionHandler(DataAccessResourceFailureException.class)
    T translate(DataAccessResourceFailureException ex);

    @ExceptionHandler(DataIntegrityViolationException.class)
    T translate(DataIntegrityViolationException ex);

    @ExceptionHandler(DataRetrievalFailureException.class)
    T translate(DataRetrievalFailureException ex);

    @ExceptionHandler(DeadlockLoserDataAccessException.class)
    T translate(DeadlockLoserDataAccessException ex);

    @ExceptionHandler(DuplicateKeyException.class)
    T translate(DuplicateKeyException ex);

    @ExceptionHandler(EmptyResultDataAccessException.class)
    T translate(EmptyResultDataAccessException ex);

    @ExceptionHandler(IncorrectResultSizeDataAccessException.class)
    T translate(IncorrectResultSizeDataAccessException ex);

    @ExceptionHandler(IncorrectUpdateSemanticsDataAccessException.class)
    T translate(IncorrectUpdateSemanticsDataAccessException ex);

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    T translate(InvalidDataAccessApiUsageException ex);

    @ExceptionHandler(InvalidDataAccessResourceUsageException.class)
    T translate(InvalidDataAccessResourceUsageException ex);

    @ExceptionHandler(NonTransientDataAccessException.class)
    T translate(NonTransientDataAccessException ex);

    @ExceptionHandler(NonTransientDataAccessResourceException.class)
    T translate(NonTransientDataAccessResourceException ex);

    @ExceptionHandler(OptimisticLockingFailureException.class)
    T translate(OptimisticLockingFailureException ex);

    @ExceptionHandler(PermissionDeniedDataAccessException.class)
    T translate(PermissionDeniedDataAccessException ex);

    @ExceptionHandler(PessimisticLockingFailureException.class)
    T translate(PessimisticLockingFailureException ex);

    @ExceptionHandler(QueryTimeoutException.class)
    T translate(QueryTimeoutException ex);

    @ExceptionHandler(RecoverableDataAccessException.class)
    T translate(RecoverableDataAccessException ex);

    @ExceptionHandler(TransientDataAccessException.class)
    T translate(TransientDataAccessException ex);

    @ExceptionHandler(TransientDataAccessResourceException.class)
    T translate(TransientDataAccessResourceException ex);

    @ExceptionHandler(TypeMismatchDataAccessException.class)
    T translate(TypeMismatchDataAccessException ex);

    @ExceptionHandler(UncategorizedDataAccessException.class)
    T translate(UncategorizedDataAccessException ex);

    @ExceptionHandler(CannotCreateTransactionException.class)
    T translate(CannotCreateTransactionException ex);

    @ExceptionHandler(HeuristicCompletionException.class)
    T translate(HeuristicCompletionException ex);

    @ExceptionHandler(IllegalTransactionStateException.class)
    T translate(IllegalTransactionStateException ex);

    @ExceptionHandler(InvalidIsolationLevelException.class)
    T translate(InvalidIsolationLevelException ex);

    @ExceptionHandler(InvalidTimeoutException.class)
    T translate(InvalidTimeoutException ex);

    @ExceptionHandler(NestedTransactionNotSupportedException.class)
    T translate(NestedTransactionNotSupportedException ex);

    @ExceptionHandler(NoTransactionException.class)
    T translate(NoTransactionException ex);

    @ExceptionHandler(TransactionException.class)
    T translate(TransactionException ex);

    @ExceptionHandler(TransactionSuspensionNotSupportedException.class)
    T translate(TransactionSuspensionNotSupportedException ex);

    @ExceptionHandler(TransactionSystemException.class)
    T translate(TransactionSystemException ex);

    @ExceptionHandler(TransactionTimedOutException.class)
    T translate(TransactionTimedOutException ex);

    @ExceptionHandler(TransactionUsageException.class)
    T translate(TransactionUsageException ex);

    @ExceptionHandler(UnexpectedRollbackException.class)
    T translate(UnexpectedRollbackException ex);
}
