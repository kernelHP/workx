package org.governator;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;

/**
 * @author he peng
 */
public class GuiceTest {

    @Test
    public void test1() throws Exception {
        Injector injector = Guice.createInjector();
        FooStarter instance = injector.getInstance(FooStarter.class);
        System.out.println(instance);
    }
}
