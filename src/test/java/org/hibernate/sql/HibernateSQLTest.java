package org.hibernate.sql;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.MySQLDialect;
import org.junit.Test;

/**
 * @author he peng
 */
public class HibernateSQLTest {

    @Test
    public void select() throws Exception {
        String[] columns = {"user_name" , "password" , "create_time" , "update_time"};

        Dialect dialect = new MySQLDialect();
        Select select = new Select(dialect);
        SelectFragment selectFragment = new SelectFragment();
        selectFragment.addColumns("user" , columns);
        select.setFromClause("user")
                .setSelectClause(selectFragment)
                .setWhereClause("id = ?");

        System.out.println(select.toStatementString());
        // select user.user_name as user_name, user.password as password, user.create_time as create_time, user.update_time as update_time from user where id = ?
    }

    @Test
    public void delete() throws Exception {
        Delete delete = new Delete();
        delete.setTableName("user")
                .addPrimaryKeyColumns(new String[]{"id"});

        System.out.println(delete.toStatementString());
        // delete from user where id=?
    }


    @Test
    public void update() throws Exception {

        String[] columns = {"user_name" , "password" , "create_time" , "update_time"};
        Dialect dialect = new MySQLDialect();
        Update update = new Update(dialect);
        update.setTableName("user")
                .setPrimaryKeyColumnNames(new String[]{"id"})
//                .addWhereColumn("id")
                .addColumns(columns);
        System.out.println(update.toStatementString());

        // update user set user_name=?, password=?, create_time=?, update_time=? where id=?
    }


    @Test
    public void insert() throws Exception {

        Dialect dialect = new MySQLDialect();
        Insert insertStatement = new Insert(dialect);
        String[] columns = {"user_id" , "user_name" , "password" , "create_time" , "update_time"};
        insertStatement.addColumns(columns);
        insertStatement.addIdentityColumn("id");
        insertStatement.setTableName("user");
        String sql = insertStatement.toStatementString();

        System.out.println(sql);
        // insert into user (user_id, user_name, password, create_time, update_time) values (?, ?, ?, ?, ?)
    }

    @Test
    public void insertSelect() throws Exception {

        Dialect dialect = new MySQLDialect();
        InsertSelect insertSelect = new InsertSelect(dialect);
        insertSelect.setTableName("user");
        Select select = new Select(dialect);
        insertSelect.setSelect(select);
        String[] columns = {"user_id" , "user_name" , "password" , "create_time" , "update_time"};

        insertSelect.addColumns(columns);
        System.out.println(insertSelect.toStatementString());

        // insert into user (user_id, user_name, password, create_time, update_time) select null from null
    }

}
