package org.hepeng.commons.util.proxy;

import lombok.Data;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.hepeng.workx.util.proxy.NativeInvoke;

import java.util.Date;

/**
 * @author he peng
 */

@Data
public class Demo1 implements Hello {

    private String msg;


//    @NativeInvoke
    public void hi() {
        System.out.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "]  hello ... ");
        testPrivate();
    }

    private void testPrivate() {
        System.err.println("test private method run");
    }

    public static void staticHi() {
        System.out.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "]  hello ... ");
    }
}
