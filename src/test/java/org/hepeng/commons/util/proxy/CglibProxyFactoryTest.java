package org.hepeng.commons.util.proxy;

import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.CallbackFilter;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import net.sf.cglib.proxy.NoOp;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.hepeng.workx.mybatis.executor.EventPublishExecutorProxy;
import org.hepeng.workx.util.proxy.CglibProxyFactory;
import org.hepeng.workx.util.proxy.Invocation;
import org.hepeng.workx.util.proxy.InvokeFilter;
import org.hepeng.workx.util.proxy.Invoker;
import org.hepeng.workx.util.proxy.JavassistProxyFactory;
import org.hepeng.workx.util.proxy.ProxyFactory;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class CglibProxyFactoryTest {

    @Test
    public void createProxy() {

        List<Invoker> invokers = new LinkedList<>();
        invokers.add(new Invoker() {
            @Override
            public Object invoke(Invocation invocation) throws Throwable {
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 1 run start .... ");
                Object invoke = invocation.invoke();
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 1 run finish .... ");
                return invoke;
            }
        });
        invokers.add(new Invoker() {
            @Override
            public Object invoke(Invocation invocation) throws Throwable {
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 2 run start .... ");
                Object invoke = invocation.invoke();
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 2 run finish .... ");
                return invoke;
            }
        });

        Demo1 target = new Demo1();
        ProxyFactory proxyFactory = new CglibProxyFactory();
        List<InvokeFilter> filters = new ArrayList<>();
        Demo1 proxy = (Demo1) proxyFactory.createProxy(Demo1.class , null, null , null, invokers , filters);
        proxy.hi();

        // static 函数不会被代理
//        proxy.staticHi();

//        System.out.println("toString -> " + proxy.toString());
//        proxy.setMsg("proxy object msg");
//
//        System.out.println("proxy invoke getMsg = " + proxy.getMsg());
//        System.out.println("native invoke getMsg = " + target.getMsg());
    }

    @Test
    public void callbacks() throws Exception {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(Demo1.class);
        enhancer.setCallbacks(new Callback[]{new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                System.out.println("0   before method run...");
                Object result = proxy.invokeSuper(obj, args);
                System.out.println("0   after method run...");
                return result;
            }
        } , new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                System.out.println("1  before method run...");
                Object result = proxy.invokeSuper(obj, args);
                System.out.println("1  after method run...");
                return result;
            }
        } , NoOp.INSTANCE});

        enhancer.setCallbackFilter(new CallbackFilter() {
            @Override
            public int accept(Method method) {
                if (Object.class.equals(method.getDeclaringClass())) {
                    return 2;
                }
                return 0;
            }
        });
        Demo1 proxy = (Demo1) enhancer.create();
//        proxy.hi();
        System.out.println("toString -> " + proxy.toString());

    }
}