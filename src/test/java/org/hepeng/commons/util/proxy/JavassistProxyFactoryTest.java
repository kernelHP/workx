package org.hepeng.commons.util.proxy;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.hepeng.workx.mybatis.executor.EventPublishExecutorProxy;
import org.hepeng.workx.util.proxy.Invocation;
import org.hepeng.workx.util.proxy.InvokeFilter;
import org.hepeng.workx.util.proxy.Invoker;
import org.hepeng.workx.util.proxy.JavassistProxyFactory;
import org.hepeng.workx.util.proxy.NativeInvoke;
import org.hepeng.workx.util.proxy.NativeInvokeAnnotationFilter;
import org.hepeng.workx.util.proxy.ObjectClassMethodInvokeFilter;
import org.hepeng.workx.util.proxy.ProxyFactory;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


public class JavassistProxyFactoryTest {

    @Test
    public void createProxy() {

        List<Invoker> invokers = new LinkedList<>();
        invokers.add(new Invoker() {
            @Override
            public Object invoke(Invocation invocation) throws Throwable {
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 1 run start .... ");
                Object invoke = invocation.invoke();
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 1 run finish .... ");
                return invoke;
            }
        });
        invokers.add(new Invoker() {
            @Override
            public Object invoke(Invocation invocation) throws Throwable {
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 2 run start .... ");
                Object invoke = invocation.invoke();
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 2 run finish .... ");
                return invoke;
            }
        });

        Demo1 target = new Demo1();
        target.setMsg("native object msg");
        ProxyFactory proxyFactory = new JavassistProxyFactory();
        List<InvokeFilter> filters = new ArrayList<>();
//        filters.add(new NativeInvokeAnnotationFilter());
//        filters.add(new ObjectClassMethodInvokeFilter());
        Demo1 proxy = (Demo1) proxyFactory.createProxy(target , null, invokers , filters);
//        proxy.hi();

        // static 函数不会被代理
//        proxy.staticHi();

//        System.out.println("toString -> " + proxy.toString());
        proxy.setMsg("proxy object msg");
//
        System.out.println("proxy invoke getMsg = " + proxy.getMsg());
        System.out.println("native invoke getMsg = " + target.getMsg());

    }
}