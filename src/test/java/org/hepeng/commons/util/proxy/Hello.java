package org.hepeng.commons.util.proxy;

/**
 * @author he peng
 */
public interface Hello {

    void hi();
}
