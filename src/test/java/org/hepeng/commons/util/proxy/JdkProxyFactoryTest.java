package org.hepeng.commons.util.proxy;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.hepeng.workx.mybatis.executor.EventPublishExecutorProxy;
import org.hepeng.workx.util.proxy.Invocation;
import org.hepeng.workx.util.proxy.InvokeFilter;
import org.hepeng.workx.util.proxy.Invoker;
import org.hepeng.workx.util.proxy.JavassistProxyFactory;
import org.hepeng.workx.util.proxy.JdkProxyFactory;
import org.hepeng.workx.util.proxy.ProxyFactory;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class JdkProxyFactoryTest {

    @Test
    public void createProxy() {

        List<Invoker> invokers = new LinkedList<>();
        invokers.add(new Invoker() {
            @Override
            public Object invoke(Invocation invocation) throws Throwable {
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 1 run start .... ");
                Object invoke = invocation.invoke();
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 1 run finish .... ");
                return invoke;
            }
        });
        invokers.add(new Invoker() {
            @Override
            public Object invoke(Invocation invocation) throws Throwable {
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 2 run start .... ");
                Object invoke = invocation.invoke();
                System.err.println("[" + DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss:SSS") + "] invoker 2 run finish .... ");
                return invoke;
            }
        });

        Demo1 target = new Demo1();
        ProxyFactory proxyFactory = new JdkProxyFactory();
        List<InvokeFilter> filters = new ArrayList<>();
        Hello proxy = (Hello) proxyFactory.createProxy(target , null, invokers , filters);
        proxy.hi();

        // static 函数不会被代理
//        proxy.staticHi();

//        System.out.println("toString -> " + proxy.toString());
    }
}