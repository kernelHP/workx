package org.hepeng.commons.crypto;

import org.apache.commons.codec.binary.StringUtils;
import org.hepeng.workx.util.crypto.RSACrypt;
import org.junit.Assert;
import org.junit.Test;

import java.nio.charset.Charset;

public class RSACryptTest {

    RSACrypt rsaCrypt = RSACrypt.newRSACrypt();

    @Test
    public void newRSACrypt() {
    }

    @Test
    public void newRSACrypt1() {
    }

    @Test
    public void generateKeyPair() {

        RSACrypt.KeyPair keyPair = rsaCrypt.generateKeyPair();
        System.out.println("PublicKey = " + keyPair.getPublicKey());
        System.out.println("PrivateKey = " + keyPair.getPrivateKey());
    }

    @Test
    public void encrypt() {

        String source = "你好";
        byte[] encrypted = rsaCrypt.encrypt("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbonaspFXlVCjhU10xBKYAO+8XT/6NlgOWbFZNdiT8/Hlny5E1yKNzBM4Al37mIXg0WnMJZfN0E598VWQTdUnYZ/np3wdYPH7aScoyhjmABfyIJ+U1ii5rq5CmVgXJ8vV0f0+m877iE9IzFyl4XM7hsXj+zOCOMufLvCaxeEmU9QIDAQAB", source.getBytes(Charset.forName("utf-8")));
        byte[] decrypted = rsaCrypt.decrypt("MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJuidqykVeVUKOFTXTEEpgA77xdP/o2WA5ZsVk12JPz8eWfLkTXIo3MEzgCXfuYheDRacwll83QTn3xVZBN1Sdhn+enfB1g8ftpJyjKGOYAF/Ign5TWKLmurkKZWBcny9XR/T6bzvuIT0jMXKXhczuGxeP7M4I4y58u8JrF4SZT1AgMBAAECgYAxtfnb+P2qq3AHUL5Set6e8oXS121CQilRMbd7NIJqACazHZV7LWPvyBgYnksdLJGdf0qBNPxHFkF50DiP8oWsRBW5TS5eQkMsJEtGbN6B5DxGysdaxJcjUE+m5g1fFNl7yELq59ZETdi1ACtPH1/ckyIJmdBhUhmtqz0j+gkZnQJBAPlCBCRqJ95zfwLn9Uo7Qmb/76l63KGKXmN33xZ8Tu01Mnhg8Rl4RVQFmfKgjR6GFuEznDMnNRV6m3D7c6pACUsCQQCf2Cdwbxcw/FJRtsdtBaFX7mi8qWh6QjFRAzPyktJ/UUeee+j5lMsyCOdaEl/3KGWdU+SBBUqnYA6XFGgE1jK/AkBUTt8Fzh40IM6qFfkipY6dNh5DLfpobVvluwrrGRq9IwwyKWADZGnWtJpQtbtlwqcEfydrKezLmg/vlC2YPVanAkAI9fswCGpKarrKre58JCapYUop2W1r/S36lq1g2e5HCO106wlihH7nrSwbFpCesHsR2NQkj4Fh5LasrY4vRzrNAkEA18q/nYfws8ETdV5A88xczu1ddL7f/wRuBjtczxkW6S7hc0zPXfz0ZYHiRvKDbIOyqGldlz63Oz1v9qEREIje6g==", encrypted);

        System.out.println("source = " + StringUtils.newStringUtf8(decrypted));
        Assert.assertTrue(org.apache.commons.lang3.StringUtils.equals(source , StringUtils.newStringUtf8(decrypted)));

    }

    @Test
    public void encryptBase64() {
        String source = "你好";
        String encryptBase64 = rsaCrypt.encryptBase64("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbonaspFXlVCjhU10xBKYAO+8XT/6NlgOWbFZNdiT8/Hlny5E1yKNzBM4Al37mIXg0WnMJZfN0E598VWQTdUnYZ/np3wdYPH7aScoyhjmABfyIJ+U1ii5rq5CmVgXJ8vV0f0+m877iE9IzFyl4XM7hsXj+zOCOMufLvCaxeEmU9QIDAQAB", source.getBytes(Charset.forName("utf-8")));
        System.out.println("encryptBase64 = " + encryptBase64);
        byte[] decrypted = rsaCrypt.decryptBase64("MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJuidqykVeVUKOFTXTEEpgA77xdP/o2WA5ZsVk12JPz8eWfLkTXIo3MEzgCXfuYheDRacwll83QTn3xVZBN1Sdhn+enfB1g8ftpJyjKGOYAF/Ign5TWKLmurkKZWBcny9XR/T6bzvuIT0jMXKXhczuGxeP7M4I4y58u8JrF4SZT1AgMBAAECgYAxtfnb+P2qq3AHUL5Set6e8oXS121CQilRMbd7NIJqACazHZV7LWPvyBgYnksdLJGdf0qBNPxHFkF50DiP8oWsRBW5TS5eQkMsJEtGbN6B5DxGysdaxJcjUE+m5g1fFNl7yELq59ZETdi1ACtPH1/ckyIJmdBhUhmtqz0j+gkZnQJBAPlCBCRqJ95zfwLn9Uo7Qmb/76l63KGKXmN33xZ8Tu01Mnhg8Rl4RVQFmfKgjR6GFuEznDMnNRV6m3D7c6pACUsCQQCf2Cdwbxcw/FJRtsdtBaFX7mi8qWh6QjFRAzPyktJ/UUeee+j5lMsyCOdaEl/3KGWdU+SBBUqnYA6XFGgE1jK/AkBUTt8Fzh40IM6qFfkipY6dNh5DLfpobVvluwrrGRq9IwwyKWADZGnWtJpQtbtlwqcEfydrKezLmg/vlC2YPVanAkAI9fswCGpKarrKre58JCapYUop2W1r/S36lq1g2e5HCO106wlihH7nrSwbFpCesHsR2NQkj4Fh5LasrY4vRzrNAkEA18q/nYfws8ETdV5A88xczu1ddL7f/wRuBjtczxkW6S7hc0zPXfz0ZYHiRvKDbIOyqGldlz63Oz1v9qEREIje6g==", encryptBase64);
        System.out.println("source = " + StringUtils.newStringUtf8(decrypted));
        Assert.assertTrue(org.apache.commons.lang3.StringUtils.equals(source , StringUtils.newStringUtf8(decrypted)));
    }

    @Test
    public void decrypt() {
    }
}