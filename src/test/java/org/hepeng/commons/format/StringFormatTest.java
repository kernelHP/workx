package org.hepeng.commons.format;

import org.junit.Test;

/**
 * @author he peng
 */
public class StringFormatTest {

    @Test
    public void test0() throws Exception {

        String format = "INSERT INTO %s %s VALUES %s";
        String s = String.format(format, "table", "(c1, c2 , c3)", "(v1 , v2 , v3)");
        System.out.println(s);

    }
}
