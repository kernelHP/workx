package org.hepeng.workx.util.timer;

import com.google.common.util.concurrent.SettableFuture;
import org.apache.commons.lang3.StringUtils;
import org.hepeng.workx.service.CommonServiceCallResult;
import org.hepeng.workx.service.ServiceCallResult;
import org.hepeng.workx.service.error.Error;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TimeoutCallerTest {

    @Test
    public void call1() {

        TimeoutCaller.call(new Function<Void>() {
            @Override
            public Void call() {
                try {
                    Thread.sleep(1000);
                    if (1 > 0) {
                        throw new RuntimeException("XSSFSFASAS");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("xxxxxxxxxxxxxxxxxxxxx");
                return null;
            }
        } , 2 , TimeUnit.SECONDS);
    }

    @Test
    public void call2() {

        ServiceCallResult<? extends Error, Object> result = TimeoutCaller.call(
                (Function<ServiceCallResult<? extends Error, Object>>)
                        () -> new CommonServiceCallResult.ServiceCallResultBuilder().error(Error.SUCCESS).build(), 2, TimeUnit.SECONDS);
        System.out.println("result = " + result);
    }

    @Test
    public void futureTest() throws Exception {
        SettableFuture<Object> future = SettableFuture.create();
        Object result = null;
        future.set(result);
        result = StringUtils.upperCase("sssssssss");
        Object o = future.get(1, TimeUnit.SECONDS);
        System.out.println(o);
    }
}