package org.hepeng.workx.spring.data.redis.serializer;

import lombok.Data;
import org.apache.commons.codec.binary.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;


/**
 * https://github.com/spring-projects/spring-session/issues/100
 */
public class SecurityModuleGenericJackson2JsonRedisSerializerTypeTest {
    RedisSerializer serializer;

    @Before
    public void init() {
        serializer = new SecurityModuleGenericJackson2JsonRedisSerializer();
        serializer = new Jackson2JsonRedisSerializer(Object.class);
        serializer = new GenericToStringSerializer(Object.class);
    }

    @Test
    public void serialize() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("ssssssssssssssss", "daskdkalda");
        TestSerialize testSerialize = new TestSerialize();
        testSerialize.setAa(11);
        testSerialize.setS("sdadasd");
        testSerialize.setMap(map);

        Map<String , Object> data = new HashMap<>();
        data.put("data" , testSerialize);
        byte[] bytes = serializer.serialize(data);
        System.out.println(StringUtils.newStringUtf8(bytes));
    }

    @Test
    public void deserialize() {
        String jsonStr = "{\"@class\":\"java.util.HashMap\",\"data\":{\"@class\":\"java.lang.Object\",\"s\":\"sdadasd\",\"aa\":11,\"map\":{\"@class\":\"java.util.HashMap\",\"ssssssssssssssss\":\"daskdkalda\"}}}\n";
        Map deserialize = (Map) serializer.deserialize(StringUtils.getBytesUtf8(jsonStr));
        Object data = deserialize.get("data");
        System.out.println(deserialize);
    }

    @Data
    public class TestSerialize {
        private String s;
        private int aa;
        private Map<String , Object> map;

    }
}