package org.hepeng.workx.spring.security.config;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.joor.Reflect;
import org.junit.Test;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;

public class SecurityExpressionUrlAuthorizationConfigHelperTest {

    @Test
    public void test0() {
        Reflect reflect = Reflect.on(ExpressionUrlAuthorizationConfigurer.class);
        Object args = new String[] {"AS" , "XC"};
        Reflect hasAnyRole = reflect.call("hasAnyRole", args);
        Object o = hasAnyRole.get();
        System.out.println(o);
    }

    @Test
    public void hasAnyRole() {
        SecurityExpressionUrlAuthorizationConfigHelper accessConfigHelper = new SecurityExpressionUrlAuthorizationConfigHelper();
        SecurityExpressionUrlAuthorizationConfigHelper securityExpressionUrlAuthorizationConfigHelper = accessConfigHelper.hasAnyRole("A", "B");
        System.out.println(securityExpressionUrlAuthorizationConfigHelper.access());
    }



    @Test
    public void hasRole() {
        SecurityExpressionUrlAuthorizationConfigHelper accessConfigHelper = new SecurityExpressionUrlAuthorizationConfigHelper();
        SecurityExpressionUrlAuthorizationConfigHelper securityExpressionUrlAuthorizationConfigHelper = accessConfigHelper.hasRole("A");
        System.out.println(securityExpressionUrlAuthorizationConfigHelper.access());
    }

    @Test
    public void hasAnyAuthority() {
        SecurityExpressionUrlAuthorizationConfigHelper accessConfigHelper = new SecurityExpressionUrlAuthorizationConfigHelper();
        SecurityExpressionUrlAuthorizationConfigHelper securityExpressionUrlAuthorizationConfigHelper = accessConfigHelper.hasAnyAuthority("A", "B");
        System.out.println(securityExpressionUrlAuthorizationConfigHelper.access());
    }

    @Test
    public void hasAuthority() {
        SecurityExpressionUrlAuthorizationConfigHelper accessConfigHelper = new SecurityExpressionUrlAuthorizationConfigHelper();
        SecurityExpressionUrlAuthorizationConfigHelper securityExpressionUrlAuthorizationConfigHelper = accessConfigHelper.hasAuthority("A");
        System.out.println(securityExpressionUrlAuthorizationConfigHelper.access());
    }

    @Test
    public void hasIpAddress() {
        SecurityExpressionUrlAuthorizationConfigHelper accessConfigHelper = new SecurityExpressionUrlAuthorizationConfigHelper();
        SecurityExpressionUrlAuthorizationConfigHelper securityExpressionUrlAuthorizationConfigHelper = accessConfigHelper.hasIpAddress("A");
        System.out.println(securityExpressionUrlAuthorizationConfigHelper.access());
    }

    @Test
    public void parse() throws Exception {
        SecurityExpressionUrlAuthorizationConfigHelper accessConfigHelper = new SecurityExpressionUrlAuthorizationConfigHelper();
        SecurityUrl securityUrl = SecurityUrl.builder()
                .not((byte) 1)
//                .anonymous((byte) 1)
//                .authenticated((byte) 1)
//                .denyAll((byte) 1)
//                .fullyAuthenticated((byte) 1)
//                .hasAnyRole("AX,SC,FG")
//                .hasAnyAuthority("AX,SC,FG")
//                .hasAuthority("XA")
//                .hasRole("FS")
//                .permitAll((byte) 1)
                .rememberMe((byte) 1)
                .build();

        Field field = securityUrl.getClass().getDeclaredField("rememberMe");
        field.setAccessible(true);
        Annotation[] annotations = field.getAnnotations();
        RememberMe annotation = field.getAnnotation(RememberMe.class);

        List<ConfigAttribute> attributeList = accessConfigHelper.parse(securityUrl);
        System.out.println(attributeList);
    }

    @Setter
    @Getter
    @Builder
    public static class SecurityUrl {

        @Not
        private Byte not;

        @PermitAll
        private Byte permitAll;

        @Anonymous
        private Byte anonymous;

        @Authenticated
        private Byte authenticated;

        @DenyAll
        private Byte denyAll;

        @FullyAuthenticated
        private Byte fullyAuthenticated;

        @HasAnyAuthority
        private String hasAnyAuthority;

        @HasAnyRole
        private String hasAnyRole;

        @HasAuthority
        private String hasAuthority;

        @HasIpAddress
        private String hasIpAddress;

        @HasRole
        private String hasRole;

        @RememberMe
        private Byte rememberMe;
    }
}