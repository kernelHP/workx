package org.hepeng.workx.mybatis.interceptor;

import org.junit.Test;

import static org.junit.Assert.*;

public class OraclePageQuerySQLRewriterTest {

    @Test
    public void doRewrite() {
        String sql = "SELECT userName " +
                " FROM comp_user  " +
                " ORDER BY userName";
        OraclePageQuerySQLRewriter sqlRewriter = new OraclePageQuerySQLRewriter();
        String rewritedSql = sqlRewriter.rewrite(sql, 5L, 5);
        System.out.println(rewritedSql);

    }
}