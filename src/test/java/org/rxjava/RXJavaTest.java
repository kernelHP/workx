package org.rxjava;

import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.BlockingObserver;
import org.hepeng.workx.util.proxy.Invocation;
import org.hepeng.workx.util.proxy.Invoker;
import org.junit.Test;
import rx.Emitter;
import rx.Observable;
import rx.Observer;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.internal.operators.BlockingOperatorToFuture;
import rx.observables.BlockingObservable;
import rx.observables.SyncOnSubscribe;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @author he peng
 */
public class RXJavaTest {

    @Test
    public void test() throws Exception {
        Observable<Object> objectObservable = Observable.create(new Action1<Emitter<Object>>() {
            @Override
            public void call(Emitter<Object> emitter) {
                emitter.requested();
            }
        }, Emitter.BackpressureMode.NONE);

        Future<Object> future = BlockingOperatorToFuture.toFuture(objectObservable);
//        Object o = future.get(5, TimeUnit.SECONDS);
//        System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhh");


        Observable<Object> observable = Observable.defer(new Func0<Observable<Object>>() {
            @Override
            public Observable<Object> call() {
                try {
                    Thread.sleep(2900);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Observable<Object> obs = Observable.just("HA HA HA");
                return obs;

            }
        });

        observable.timeout(3, TimeUnit.SECONDS);

        observable.subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                System.out.println("result = " + o);
            }
        });

//        future = BlockingOperatorToFuture.toFuture(observable);
//        Object o = future.get(5, TimeUnit.SECONDS);
//        System.out.println("result = " + o);
    }

    interface Supplyer<T> {

        T supply();
    }
}
