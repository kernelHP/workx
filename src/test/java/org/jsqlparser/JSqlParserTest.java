package org.jsqlparser;

import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.SignedExpression;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.util.deparser.ExpressionDeParser;
import net.sf.jsqlparser.util.deparser.SelectDeParser;
import org.hepeng.workx.sqlparse.CountItem;
import org.hepeng.workx.sqlparse.SelectParser;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author he peng
 */
public class JSqlParserTest {

    @Test
    public void replacingStringvalues() throws Exception {
        String sql ="SELECT NAME, ADDRESS, COL1 FROM USER WHERE " +
                "SSN IN ('11111111111111', '22222222222222') " +
                " AND a = 2 " +
                "ORDER BY NAME;";
        Select select = (Select) CCJSqlParserUtil.parse(sql);

        //Start of value modification
        StringBuilder buffer = new StringBuilder();
        ExpressionDeParser expressionDeParser = new ExpressionDeParser() {

        };
        SelectDeParser deparser = new SelectParser(expressionDeParser,buffer);
        expressionDeParser.setSelectVisitor(deparser);
        expressionDeParser.setBuffer(buffer);
        PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
        List<SelectItem> selectItems = new ArrayList<>();
//        SelectItem selectItem = new CountItem();
//        selectItems.add(selectItem);

        Column c = new Column();
        c.setColumnName("ROWNUM");
        Alias alias = new Alias("rowno");
        SelectExpressionItem selectExpressionItem = new SelectExpressionItem(c);
        selectExpressionItem.setAlias(alias);

        List<SelectItem> selectItems1 = plainSelect.getSelectItems();
        selectItems1.add(selectExpressionItem);

        plainSelect.setSelectItems(selectItems1);

        Expression where = plainSelect.getWhere();
        Column column = new Column();
        column.setColumnName("XXX");
        LongValue longValue = new LongValue(20);

        MinorThanEquals minorThanEquals = new MinorThanEquals();
        minorThanEquals.setLeftExpression(column);
        minorThanEquals.setRightExpression(longValue);
        AndExpression andExpression = new AndExpression(where , minorThanEquals);
        plainSelect.setWhere(andExpression);

//        List<OrderByElement> orderByElements = plainSelect.getOrderByElements();
        plainSelect.setOrderByElements(null);
        plainSelect.accept(deparser);
        //End of value modification


        System.out.println(buffer.toString());
    }
}
