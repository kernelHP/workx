package org.redis;

import org.apache.commons.codec.binary.StringUtils;
import org.junit.Before;
import org.junit.Test;
import sun.net.util.IPAddressUtil;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author he peng
 */
public class RedisClientTest {

    Socket socket;
    InputStream inputStream;
    OutputStream outputStream;
    String key = "redis-client-test";

    @Test
    public void test() {
        String url = "http://smp.p2phz.com/xb12369";
//        String url = "http://10.10.10.183/xb12369";

        Pattern p = Pattern.compile("(?<=http://|\\.)[^.]*?\\.(com|cn|net|org|biz|info|cc|tv)",Pattern.CASE_INSENSITIVE);
        Matcher matcher = p.matcher(url);
        matcher.find();
        System.out.println("域名是："+matcher.group());
        if (matcher.matches()) {
            System.out.println("域名是："+matcher.group(1));
        }
        System.out.println(IPAddressUtil.isIPv4LiteralAddress("10.10.10.183"));
    }

    @Before
    public void init() throws Exception {
        socket = new Socket("10.10.10.183", 6379);
        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();
    }

    @Test
    public void set() throws Exception {

        String value = "test";
        StringBuilder sb = new StringBuilder();
        sb.append("*3").append("\r\n");
        sb.append("$3").append("\r\n");
        sb.append("SET").append("\r\n");
        sb.append("$").append(key.length()).append("\r\n");
        sb.append(key).append("\r\n");
        sb.append("$").append(value.length()).append("\r\n");
        sb.append(value).append("\r\n");

        byte[] bytes= new byte[1024];
        outputStream.write(sb.toString().getBytes());
        int len;
        while ((len = inputStream.read(bytes)) != -1) {
            String s = new String(bytes, 0, len, "utf-8");
            System.out.println(s);
            System.out.println(s.length());
        }
    }

    @Test
    public void get() throws Exception {

        StringBuilder sb = new StringBuilder();
        sb.append("*2").append("\r\n");
        sb.append("$3").append("\r\n");
        sb.append("GET").append("\r\n");
        sb.append("$").append(key.length()).append("\r\n");
        sb.append(key).append("\r\n");

        byte[] bytes= new byte[1024];
        outputStream.write(sb.toString().getBytes());
        int len;
        while ((len = inputStream.read(bytes)) != -1) {
            String s = new String(bytes, 0, len, "utf-8");
            System.out.println(s);
            System.out.println(s.length());
        }
    }
}
